﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
/// <summary>
/// Updates the money text in the inventory.
/// </summary>
public class MoneyInInventory : MonoBehaviour {

	/// <summary>
	/// Updates the money text in the inventory.
	/// </summary>
	void Update () {
		GetComponent<Text>().text = "Money in pocket: "+Master.Instance.money;
	}
}
