﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Handles the behaviour and lifetime of this specific object.
/// </summary>
public class AnimatePlus : MonoBehaviour {
	/// <summary>
	/// Is saved when the item is instantiated, update times when it needs to be destroyed again.
	/// </summary>
	float time;
	/// <summary>
	/// Sets the time variable to be the current time + 2 seconds.
	/// </summary>
	void Start () {
		time = Master.Instance.gameValues.getTime()+2f;
	}
	
	/// <summary>
	/// Checks for when 2 seconds has passed since this object has been spawned. After then, it'll destroy itself.
	/// </summary>
	void Update () {
		if(Master.Instance.gameValues.getTime() > time)
		{
			DestroyImmediate(this.gameObject);
		}
		else
		{
			transform.position += new Vector3(0f,0.1f*Time.deltaTime,0f);
		}
	
	}
}
