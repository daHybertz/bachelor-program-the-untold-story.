﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Handles all the hospital functionality.
/// </summary>
public class Hospital : MonoBehaviour {
	/// <summary>
	/// Stores the player gameObject for ease of reference in this class.
	/// </summary>
	GameObject Player;
	/// <summary>
	/// if the player is at the hospital. This is true.
	/// </summary>
	bool isPlayerAtHospital;
	/// <summary>
	/// defines the maximum amount of uses the hospital can be used in the game.
	/// </summary>
	int hospitalUses = 3;
	/// <summary>
	/// increments when the hospital is used to keep track of hospital uses.	/// </summary>
	int uses;
	/// <summary>
	/// Initializes the variables so they are ready for use.
	/// </summary>
	void Start () {
		uses = 0;
		Player = GameObject.FindGameObjectWithTag("Player");
		isPlayerAtHospital = false;
	}
	
	/// <summary>
	/// This continually checks for if the player passes out and needs to be sent to the hospital.
	/// </summary>
	void Update () {

		if(Master.Instance.gameValues.getSustenance() < 0.08f && hospitalUses >= uses && !isPlayerAtHospital)
		{
			Master.Instance.gameValues.setHygiene(2);
			Master.Instance.gameValues.setHeat(1);
			Master.Instance.gameValues.setSustenance(1);
			Master.Instance.gameValues.UpdateAllValues();
			OutputText.Instance.NewMessage("You passed out from malnourishment. Remember to eat.","#FF0000");
			Player.transform.position = this.transform.position;
			Master.Instance.FadeOut = true;
			Master.Instance.fadeSpeed = 2f;
			isPlayerAtHospital = true;
			hospitalUses++;
		}
		if(Master.Instance.gameValues.getCurrDrugUnits() > 150 && hospitalUses >= uses && !isPlayerAtHospital)
		{
			Master.Instance.gameValues.setHygiene(2);
			Master.Instance.gameValues.setHeat(1);
			Master.Instance.gameValues.setSustenance(1);
			Master.Instance.gameValues.resetDrugs();
			Master.Instance.drugsOnPerson = 0;
			OutputText.Instance.NewMessage("You fainted from an overdose. Don't take too may drugs at one time.","#00FF00");
			Player.transform.position = this.transform.position;
			Master.Instance.FadeOut = true;
			Master.Instance.fadeSpeed = 1f;
			isPlayerAtHospital = true;
			hospitalUses++;
		}
	
	}
}
