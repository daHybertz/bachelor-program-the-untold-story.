using UnityEngine;
using UnityEngine.UI;
using System.Collections;
/// <summary>
/// Handles all the mall & kiosk UI funcitonality.
/// </summary>
public class BuyFromMall : MonoBehaviour {
	/// <summary>
	/// displays the price of a shaver.
	/// </summary>
	public Text shaver;
	/// <summary>
	/// displays the price of soap.
	/// </summary>
	public Text soap;
	/// <summary>
	/// displays the price of deodorant.
	/// </summary>
	public Text deo;
	/// <summary>
	/// displays the price of a toothbrush.
	/// </summary>
	public Text tBrush;
	/// <summary>
	/// displays the price of a beer.
	/// </summary>
	public Text beer;
	/// <summary>
	/// displays the price of a sleepingbag.
	/// </summary>
	public Text bag;
	/// <summary>
	/// displays the return money from recycling bottles.
	/// </summary>
	public Text t;
	/// <summary>
	/// displays the money a player has on his person.
	/// </summary>
	public Text moneyText;
	/// <summary>
	/// displays the price of a storebought meal.
	/// </summary>
	public Text storeBoughtPrice;
	/// <summary>
	/// modifies the price of anything based on whether or not it's a kiosk or the mall. 
	/// </summary>
	public float modifier;
	/// <summary>
	/// contains the reference to the player.
	/// </summary>
	public GameObject p;

	/// <summary>
	/// initializes all the text variables above, and updates them with the right price.
	/// </summary>
	public void Start()
	{
		shaver.text = "<b> Costs: "+(int)(Master.Instance.shaver.getPrice()*modifier)+" Euros</b>";
		soap.text = "<b> Costs: "+(int)(Master.Instance.soap.getPrice()*modifier)+" Euros</b>";
		deo.text = "<b> Costs: "+(int)(Master.Instance.deo.getPrice()*modifier)+" Euros</b>";
		tBrush.text = "<b> Costs: "+(int)(Master.Instance.toothbrush.getPrice()*modifier)+" Euros</b>";
		beer.text = "<b> Costs: "+(int)(Master.Instance.alcohol.getPrice()*modifier)+" Euros</b>";
		bag.text = "<b> Costs: "+Master.Instance.sleepingBag.getPrice()+" Euros</b>";
		storeBoughtPrice.text = "<b> Costs: "+(int)(Master.Instance.storebought.getPrice()*modifier)+" Euros</b>";
	}
	/// <summary>
	/// continually updates the money on person textbox.
	/// </summary>
	public void Update()
	{
		moneyText.text = "Money in pocket: "+Master.Instance.money;
	}
	/// <summary>
	/// handles the buying of a shaver functionality.
	/// </summary>
	public void OnBuyShaver()
	{
		float r = Random.Range(-50,50);
		r = r/100;
		if((int)(Master.Instance.shaver.getPrice()*modifier) <= Master.Instance.money)
		{
			GameObject a =
				Instantiate(p, new Vector3(shaver.gameObject.transform.position.x+r,shaver.gameObject.transform.position.y,shaver.gameObject.transform.position.z) ,Quaternion.identity)
					as GameObject;
			if(modifier > 1f)
			{
				transform.parent.FindChild("ExitBtnKiosk").GetComponent<ExitShop>().pp.Add(a);
			}
			else
				transform.parent.FindChild("ExitBtn").GetComponent<ExitShop>().pp.Add(a);
			Master.Instance.money -= (int)(Master.Instance.shaver.getPrice()*modifier);
			Master.Instance.shaverOnPerson++;
			
		}
	}
	/// <summary>
	/// handles the buying of soap functionality.
	/// </summary>
	public void OnBuySoap()
	{
		float r = Random.Range(-50,50);
		r = r/100;
		if((int)(Master.Instance.soap.getPrice()*modifier) <= Master.Instance.money)
		{
			GameObject a =
				Instantiate(p, new Vector3(soap.gameObject.transform.position.x+r,soap.gameObject.transform.position.y,soap.gameObject.transform.position.z) ,Quaternion.identity)
					as GameObject;
			if(modifier > 1f)
			{
				transform.parent.FindChild("ExitBtnKiosk").GetComponent<ExitShop>().pp.Add(a);
			}
			else
				transform.parent.FindChild("ExitBtn").GetComponent<ExitShop>().pp.Add(a);
			Master.Instance.money -= (int)(Master.Instance.soap.getPrice()*modifier);
			Master.Instance.soapOnPerson++;
			
		}
	}
	/// <summary>
	/// handles the buying of deodorant functionality.
	/// </summary>
	public void OnBuyDeo()
	{
		float r = Random.Range(-50,50);
		r = r/100;
		if((int)(Master.Instance.deo.getPrice()*modifier) <= Master.Instance.money)
		{
			GameObject a =
				Instantiate(p, new Vector3(deo.gameObject.transform.position.x+r,deo.gameObject.transform.position.y,deo.gameObject.transform.position.z) ,Quaternion.identity)
					as GameObject;
			if(modifier > 1f)
			{
				transform.parent.FindChild("ExitBtnKiosk").GetComponent<ExitShop>().pp.Add(a);
			}
			else
				transform.parent.FindChild("ExitBtn").GetComponent<ExitShop>().pp.Add(a);
			Master.Instance.money -= (int)(Master.Instance.deo.getPrice()*modifier);
			Master.Instance.deoOnPerson++;
			
		}
	}
	/// <summary>
	/// handles the buying of a toothbrush functionality.
	/// </summary>
	public void OnBuyToothBrush()
	{
		float r = Random.Range(-50,50);
		r = r/100;
		if((int)(Master.Instance.toothbrush.getPrice()*modifier) <= Master.Instance.money)
		{
			GameObject a =
				Instantiate(p, new Vector3(tBrush.gameObject.transform.position.x+r,tBrush.gameObject.transform.position.y,tBrush.gameObject.transform.position.z) ,Quaternion.identity)
					as GameObject;
			if(modifier > 1f)
			{
				transform.parent.FindChild("ExitBtnKiosk").GetComponent<ExitShop>().pp.Add(a);
			}
			else
				transform.parent.FindChild("ExitBtn").GetComponent<ExitShop>().pp.Add(a);
			Master.Instance.money -= (int)(Master.Instance.toothbrush.getPrice()*modifier);
			Master.Instance.toothbrushOnPerson = 800;
			
		}
	}
	/// <summary>
	/// handles the buying of buying alcohol functionality.
	/// </summary>
	public void OnBuyAlcohol()
	{
		float r = Random.Range(-50,50);
		r = r/100;
		if((int)(Master.Instance.alcohol.getPrice()*modifier) <= Master.Instance.money)
		{
			GameObject a =
				Instantiate(p, new Vector3(beer.gameObject.transform.position.x+r,beer.gameObject.transform.position.y,beer.gameObject.transform.position.z) ,Quaternion.identity)
					as GameObject;
			if(modifier > 1f)
			{
				transform.parent.FindChild("ExitBtnKiosk").GetComponent<ExitShop>().pp.Add(a);
			}
			else
				transform.parent.FindChild("ExitBtn").GetComponent<ExitShop>().pp.Add(a);
			Master.Instance.money -= (int)(Master.Instance.alcohol.getPrice()*modifier);
			Master.Instance.alcoOnPerson++;
			
		}
	}
	/// <summary>
	/// handles the buying of a sleepingbag functionality.
	/// </summary>
	public void OnBuySleepingBag()
	{
		float r = Random.Range(-50,50);
		r = r/100;
		if(Master.Instance.sleepingBag.getPrice() <= Master.Instance.money)
		{
			GameObject a =
				Instantiate(p, new Vector3(bag.gameObject.transform.position.x+r,bag.gameObject.transform.position.y,bag.gameObject.transform.position.z) ,Quaternion.identity)
					as GameObject;
			if(modifier > 1f)
			{
				transform.parent.FindChild("ExitBtnKiosk").GetComponent<ExitShop>().pp.Add(a);
			}
			else
				transform.parent.FindChild("ExitBtn").GetComponent<ExitShop>().pp.Add(a);
			Master.Instance.money -= Master.Instance.sleepingBag.getPrice();
			Master.Instance.sleepingBagOnPerson = 800;
			
		}
	}
	/// <summary>
	/// handles the recycling functionality.
	/// </summary>
	public void OnRecycle()
	{
		float r = Random.Range(-50,50);
		r = r/100;
		if(Master.Instance.bottles > 0)
		{
			t.text = "<b>Recieved: "+Master.Instance.bottles+" euros from recycling </b>";
			Master.Instance.money += Master.Instance.bottles*3;
			Master.Instance.bottles = 0;
			StartCoroutine("textTime");
		}
	}
	/// <summary>
	/// poor excuse for a quick fix. using this as a timer. This is not an optimal solution, but it is fast.
	/// </summary>
	public IEnumerator textTime()
	{
		yield return new WaitForSeconds(3f);
		t.text = "";
		StopCoroutine("textTime");

	}
	/// <summary>
	/// handles the buying of a meal functionality.
	/// </summary>
	public void OnBuyMeal()
	{
		float r = Random.Range(-50,50);
		r = r/100;
		if((int)(Master.Instance.storebought.getPrice()*modifier) <= Master.Instance.money)
		{
			GameObject a =
				Instantiate(p, new Vector3(storeBoughtPrice.gameObject.transform.position.x+r,
				                           storeBoughtPrice.gameObject.transform.position.y,
				                           storeBoughtPrice.gameObject.transform.position.z), Quaternion.identity)
					as GameObject;
			if(modifier > 1f)
			{
				transform.parent.FindChild("ExitBtnKiosk").GetComponent<ExitShop>().pp.Add(a);
			}
			else
				transform.parent.FindChild("ExitBtn").GetComponent<ExitShop>().pp.Add(a);
			Master.Instance.money -= (int)(Master.Instance.storebought.getPrice()*modifier);
			Master.Instance.storeboughtFood++;
			
		}
	}
}
