﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// A script made for buttonpush functionality.
/// </summary>
public class ExitShop : MonoBehaviour {
	/// <summary>
	/// Holds all the animated plus objects from when items are bought in a shop UI. 
	/// Storing them here allows for quick destruction upon UI closing./// </summary>
	public List<GameObject> pp;
	/// <summary>
	/// intializes the variables.
	/// </summary>
	public void Start()
	{
		pp = new List<GameObject>();
	}
	/// <summary>
	/// Closes the shop UI in question.
	/// </summary>
	public void CloseShop()
	{
		foreach(GameObject h in pp)
		{
			Destroy(h);
		}
		Master.Instance.inUI = false;
		transform.parent.gameObject.SetActive(false);
	}
}
