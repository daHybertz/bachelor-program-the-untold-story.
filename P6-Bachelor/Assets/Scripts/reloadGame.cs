﻿using UnityEngine;
using System.Collections;
/// <summary>
/// A script made for buttonpush functionality.
/// </summary>
public class reloadGame : MonoBehaviour {

	/// <summary>
	/// If the corresponding button is pushed. This will reload the current level
	/// </summary>
	public void replay()
	{
		Application.LoadLevel(0);
	}
}
