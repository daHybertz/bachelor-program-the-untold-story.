﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
/// <summary>
/// A class for handling text input into the OutputText box.
/// </summary>
class textInput
{
	/// <summary>
	/// Color of string.
	/// </summary>
	public string type;
	/// <summary>
	/// Value of string.
	/// </summary>
	public string txt;
	/// <summary>
	/// Initializes a new instance of the <see cref="textInput"/> class.
	/// </summary>
	/// <param name="myType">Color of string..</param>
	/// <param name="myTxt">value of string.</param>
	public textInput(string myType, string myTxt)
	{
		type = myType;
		txt = myTxt;
	}
}
/// <summary>
/// Handles the text feedback shown on the screen in a text box./// </summary>
public class OutputText : MonoBehaviour{
	/// <summary>
	/// This is a singleton like the Master script. This allows all scripts access.
	/// </summary>
	public static OutputText Instance;
	/// <summary>
	/// The maximum amount of lines there can be in the final string at any one time.
	/// </summary>
	private int maxLines;
	/// <summary>
	/// Make a queue for the lines to be stored in, before they are merged into one collective string.
	/// </summary>
	private Queue<textInput> queue;
	/// <summary>
	/// This is the collective string shown in the output text.
	/// </summary>
	private string Mytext;
	/// <summary>
	/// Makes sure there can only be one OutputText object at a time.
	/// </summary>
	void Awake()
	{
		//Make sure there is only one Master class - If there is more, then the gameValues won't update properly.
		if(Instance != null)
		{
			Destroy (this);
		}
		else
		{
			Instance = this;
		}
	}
	/// <summary>
	/// Initializes a new instance of the <see cref="OutputText"/> class.
	/// </summary>
	public OutputText()
	{
		maxLines = 16;
		queue = new Queue<textInput>();
		Mytext = "";
	}
	/// <summary>
	/// Everytime a new message is needed, pass it through this function.
	/// </summary>
	/// <param name="message">Message.</param>
	/// <param name="type">Color of string.</param>
	public void NewMessage(string message, string type) {
		if (queue.Count >= maxLines)
			queue.Dequeue();

		textInput n = new textInput(type, message);

		queue.Enqueue(n);
		Mytext = "";
		foreach (textInput st in queue)
		{
			Mytext = Mytext + "<color="+st.type+">"+st.txt+"</color>" + "\n";
		}

	}
	

	// Update is called once per frame
	public void Update () 
	{
		gameObject.GetComponent<Text>().text = Mytext;
	}
}
