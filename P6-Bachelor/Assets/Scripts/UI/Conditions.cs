﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Handles the visual feedback in relation to player conditions.
/// </summary>
public class Conditions : MonoBehaviour {
	/// <summary>
	/// This contains a reference for the feedbackCold gameObject.
	/// </summary>
	public GameObject FeedbackCold;
	/// <summary>
	/// TThis contains a reference for the feedbackHungry gameObject.
	/// </summary>
	public GameObject FeedbackHungry;
	/// <summary>
	/// TThis contains a reference for the feedbackTired gameObject.
	/// </summary>
	public GameObject FeedbackTired;
	/// <summary>
	/// TThis contains a reference for the feedbackInteraction gameObject.
	/// </summary>
	public GameObject FeedbackInteraction;
	/// <summary>
	/// A boolean array of feedback cold. 
	/// Used to escalate the feedback when the values become more critical.
	/// </summary>
	bool[] feedbackCold;
	/// <summary>
	/// A boolean array of feedback Hygiene. 
	/// Used to escalate the feedback when the values become more critical.
	/// </summary>
	bool[] feedbackHygiene;
	/// <summary>
	/// A boolean array of feedbackHungry. 
	/// Used to escalate the feedback when the values become more critical.
	/// </summary>
	bool[] feedbackHungry;
	/// <summary>
	/// if true, the game feeds back tired to the player.
	/// </summary>
	bool feedbackTired;
	/// <summary>
	/// if true, the game feeds back the interaction option to the player.
	/// </summary>
	bool interaction;

	/// <summary>
	/// Initializes the variables needed for this script.
	/// </summary>
	void Start () {
		feedbackCold = new bool[2];
		feedbackHygiene = new bool[3];
		feedbackHungry = new bool[3];
		feedbackTired = false;
		feedbackBrokenLegs = false;
		interaction = false;
	}
	
	/// <summary>
	/// Updates the player with various visual feedback.
	/// </summary>
	void Update () {
		#region hungry
		if(Master.Instance.gameValues.getSustenance() < 0.35f && Master.Instance.gameValues.getSustenance() > 0.25f && !feedbackHungry[0])
		{
			OutputText.Instance.NewMessage("You feel quite hungry, maybe you should eat?", "#FF5151");
			feedbackHungry[0] = true;
		}
		else if(Master.Instance.gameValues.getSustenance() < 0.25f && Master.Instance.gameValues.getSustenance() > 0.15f && !feedbackHungry[1])
		{
			OutputText.Instance.NewMessage("You feel famished, get some food.", "#DD1F1F");
			feedbackHungry[0] = false;
			feedbackHungry[1] = true;
		}
		else if(Master.Instance.gameValues.getSustenance() < 0.15f && !feedbackHungry[2])
		{
			OutputText.Instance.NewMessage("If you don't eat soon you'll faint from starvation", "#FF0000");
			feedbackHungry[1] = false;
			feedbackHungry[2] = true;
		}
		else
		{
			if(Master.Instance.gameValues.getSustenance() > 0.50f && feedbackHungry[1])
			{
				OutputText.Instance.NewMessage("You are now fed.", "#FF5151");
				feedbackHungry[0] = false;
				feedbackHungry[1] = false;
				feedbackHungry[2] = false;
			}
		}
		#endregion hungry
		#region cold
		if(Master.Instance.gameValues.getHeat() < 0.2f && !feedbackCold[0])
		{
			OutputText.Instance.NewMessage("It's cold, you should consider finding somewhere warm.", "#0000FF");
			feedbackCold[0] = true;
		}
		else if(Master.Instance.gameValues.getHeat() <= 0.1f && !feedbackCold[1])
		{
			if(FeedbackCold.GetComponent<SpriteRenderer>().enabled == false)
			{
				FeedbackCold.GetComponent<SpriteRenderer>().enabled = true;
			}

			OutputText.Instance.NewMessage("It's dead cold out here. Get some heat or you'll suffer hypothermia.", "#00BFFF");
			feedbackCold[1] = true;

			if(Master.Instance.gameValues.getHeat() > 0.1f)
			{
				FeedbackCold.GetComponent<SpriteRenderer>().enabled = false;
			}
		}
		#endregion cold
		#region hygiene
		if(Master.Instance.gameValues.getHygiene() < 0f && !feedbackHygiene[0])
		{
			OutputText.Instance.NewMessage("It's been a while since you last had a good wash", "#F6FF00");
			feedbackHygiene[0] = true;
		}
		else if(Master.Instance.gameValues.getHygiene() < -0.2f && !feedbackHygiene[1])
		{
			OutputText.Instance.NewMessage("This is getting unhealthy... Be wary of infections.", "#B4D400");
			feedbackHygiene[1] = true;
		}
		else if(Master.Instance.gameValues.getHygiene() < -0.4f && !feedbackHygiene[2])
		{
			OutputText.Instance.NewMessage("If you don't clean yourself up soon, you risk serious health issues.", "#AAFF00");
			feedbackHygiene[2] = true;
		}
		else
		{
			if(Master.Instance.gameValues.getHygiene() > 0.1f)
			{
				feedbackHygiene[0] = false;
				feedbackHygiene[1] = false;
				feedbackHygiene[2] = false;
			}
		}
		#endregion hygiene
		#region tired
		if(Master.Instance.gameValues.getTired())
		{
			if(FeedbackTired.GetComponent<SpriteRenderer>().enabled == false && !feedbackTired)
			{
				OutputText.Instance.NewMessage("You've been up for a while, you should consider sleeping", "#FFFF00");
				FeedbackTired.GetComponent<SpriteRenderer>().enabled = true;
				feedbackTired = true;
			}
		}
		else
		{
			if(!Master.Instance.gameValues.getTired() && feedbackTired)
			{
				FeedbackTired.GetComponent<SpriteRenderer>().enabled = false;
				feedbackTired = false;
			}
		}
		#endregion tired
		#region interaction
		if(GetComponent<InteractionEvent>().feedbackInteraction)
		{
			if(FeedbackInteraction.GetComponent<SpriteRenderer>().enabled == false && interaction == false)
			{
				FeedbackInteraction.GetComponent<SpriteRenderer>().enabled = true;
				interaction = true;
			}
		}
		else
		{
			if(!GetComponent<InteractionEvent>().feedbackInteraction)
			{
				FeedbackInteraction.GetComponent<SpriteRenderer>().enabled = false;
				interaction = false;
			}
		}
		#endregion interaction
	}
}
