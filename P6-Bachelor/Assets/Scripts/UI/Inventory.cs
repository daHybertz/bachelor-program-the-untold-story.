using UnityEngine;
using UnityEngine.UI;
using System.Collections;
/// <summary>
/// Handles the inventory functionality.
/// </summary>
public class Inventory : MonoBehaviour {
	/// <summary>
	/// if this is true, inventory is active. If this is false, inventory is inactive.
	/// </summary>
	bool active;
	/// <summary>
	/// References the backpack GameObject.
	/// </summary>
	GameObject bag;
	/// <summary>
	/// References the background GameObject.
	/// </summary>
	GameObject bg;
	/// <summary>
	/// A sprite containing the manyBottles image for when there are more than 5 bottles in the inventory.
	/// </summary>
	public Sprite manyBottles;
	/// <summary>
	/// A sprite containing the bottle image for when there are less than 5 bottles in the inventory.
	/// </summary>
	public Sprite bottle;

	/// <summary>
	/// Initializes the variables needed for this script.
	/// </summary>
	void Start () {
		active = false;
		bag = transform.parent.FindChild("InventoryOpen").gameObject;
		bg = transform.parent.FindChild("background").gameObject;
	}
	
	/// <summary>
	/// If the player presses I or B the inventory should open or close.
	/// </summary>
	void Update () {
		if(Input.GetKeyDown (KeyCode.I))
		{
			OpenInventory();
		}
		if(Input.GetKeyDown (KeyCode.B))
		{
			OpenInventory();
		}
	}
	/// <summary>
	/// When this button is pushed, the inventory opens. 
	/// It then checks if the player has any objects on him and enables the buttons	in the inventory./// </summary>
	public void OpenInventory()
	{
		active = !active;
		bag.SetActive(active);
		bg.SetActive(active);
		if(Master.Instance.drugsOnPerson > 0)
		{
			bag.transform.FindChild("PillButton").gameObject.SetActive(active);
		}
		else
		{
			bag.transform.FindChild("PillButton").gameObject.SetActive(false);
		}
		if(Master.Instance.alcoOnPerson > 0)
		{
			bag.transform.FindChild("AlcoholButton").gameObject.SetActive(active);
		}
		else
		{
			bag.transform.FindChild("AlcoholButton").gameObject.SetActive(false);
		}
		if(Master.Instance.storeboughtFood > 0)
		{
			bag.transform.FindChild("HotdogButton").gameObject.SetActive(active);
		}
		else
		{
			bag.transform.FindChild("HotdogButton").gameObject.SetActive(false);
		}
		if(Master.Instance.deoOnPerson > 0)
		{
			bag.transform.FindChild("Deodorant").gameObject.SetActive(active);
		}
		else
		{
			bag.transform.FindChild("Deodorant").gameObject.SetActive(false);
		}
		if(Master.Instance.shaverOnPerson > 0)
		{
			bag.transform.FindChild("Shaver").gameObject.SetActive(active);
		}
		else
		{
			bag.transform.FindChild("Shaver").gameObject.SetActive(false);
		}
		if(Master.Instance.soapOnPerson > 0)
		{
			bag.transform.FindChild("Soap").gameObject.SetActive(active);
		}
		else
		{
			bag.transform.FindChild("Soap").gameObject.SetActive(false);
		}
		if(Master.Instance.toothbrushOnPerson > 0)
		{
			bag.transform.FindChild("Toothbrush").gameObject.SetActive(active);
		}
		else
		{
			bag.transform.FindChild("Toothbrush").gameObject.SetActive(false);
		}
		if(Master.Instance.trashOnPerson > 0)
		{
			bag.transform.FindChild("TrashFoodBtn").gameObject.SetActive(active);
		}
		else
		{
			bag.transform.FindChild("TrashFoodBtn").gameObject.SetActive(false);
		}
		if(Master.Instance.sleepingBagOnPerson > 0)
		{
			bag.transform.FindChild("SleepingBag").gameObject.SetActive(active);
		}
		else
		{
			bag.transform.FindChild("SleepingBag").gameObject.SetActive(false);
		}
		if(Master.Instance.bottles > 0)
		{

			bag.transform.FindChild("Bottles").gameObject.SetActive(active);
			bag.transform.FindChild("Bottles").transform.GetChild(0).GetComponent<Text>().text = ""+Master.Instance.bottles;
			if(Master.Instance.bottles > 5)
			{
				bag.transform.FindChild("Bottles").gameObject.GetComponent<Image>().sprite = manyBottles;
			}
			else
				bag.transform.FindChild("Bottles").gameObject.GetComponent<Image>().sprite = bottle;
		
		}
		else
		{
			bag.transform.FindChild("Bottles").gameObject.SetActive(false);
		}
	}
}
