﻿Shader "Custom/DepthMapping" {
Properties {
			_MainTex ("Base (RGB)", 2D) = "white" {} // Main Texture
		}
 
	SubShader {
		// Render the mask after regular geometry, but before masked geometry and
		// transparent things.
 
		Tags {"Queue" = "Geometry+10" }
 
		// Don't draw in the RGBA channels; just the depth buffer
 
		ColorMask 0
		ZWrite On
 
		// Do nothing specific in the pass:
 
		Pass {}
	}

	FallBack "Diffuse"
}
