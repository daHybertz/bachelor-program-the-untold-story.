﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Core functionality for all drug objects.
/// </summary>
public class Intoxicants : Expendables {

	/// <summary>
	/// The amount of drugunits this intoxicant is worth.
	/// </summary>
	private int toxValue;

	/// <summary>
	/// The amount of time this Intoxicant adds to the detoxTime.
	/// </summary>
	private int toxTime;

	/// <summary>
	/// Gets the tox value.
	/// </summary>
	public int getToxValue()
	{
		return toxValue;
	}
	/// <summary>
	/// Gets the tox time.
	/// </summary>
	public int getToxTime()
	{
		return toxTime;
	}
	/// <summary>
	/// Initializes a new instance of the <see cref="Intoxicants"/> class.
	/// </summary>
	/// <param name="myPrice">Price of one entity.</param>
	/// <param name="myType">Type of this drug.</param>
	/// <param name="myName">Name of this drug.</param>
	/// <param name="myToxValue">drug unit value of this drug.</param>
	/// <param name="myToxTime">tox time added from this drug.</param>
	public Intoxicants(int myPrice, string myType, string myName, int myToxValue, int myToxTime)
	{
		setPrice(myPrice);
		setType(myType);
		setName(myName);
		toxValue = myToxValue;
		toxTime = myToxTime;
	}
	/// <summary>
	/// Called when the Item is used to initialize its properties.
	/// </summary>
	/// <param name="value">Value.</param>
	public override void Use (ref int value)
	{
			//initialise everything that need be done when drugs are used.
			if(value > 0)
			{
				value--;
				Intoxicate();
			}
	}
	/// <summary>
	/// Intoxicates the player with this drugs values.
	/// </summary>
	private void Intoxicate()
	{
		Debug.Log ("Intoxicate: " + getNameExpend());
		Master.Instance.gameValues.setDrugUnits(toxValue);
		Master.Instance.gameValues.setCurrDrugUnits(toxValue);
		Master.Instance.gameValues.setHasPlayerTakenDrugs(true);
		Master.Instance.gameValues.setDetoxTime(toxTime);
		Master.Instance.gameValues.UpdateAllValues();
	}

}
