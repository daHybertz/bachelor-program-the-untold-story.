﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Core functionality for all food objects.
/// </summary>
public class Food : Expendables {
	/// <summary>
	/// Holds the sustenance Modfier.
	/// </summary>
	private float sustenanceMOD;
	/// <summary>
	/// Holds the foodQuality value.
	/// </summary>
	private float foodQuality;
	/// <summary>
	/// if this is true, then the food is hot.
	/// </summary>
	private bool hot;
	/// <summary>
	/// Initializes a new instance of the <see cref="Food"/> class.
	/// </summary>
	/// <param name="myPrice">Price of the object.</param>
	/// <param name="myType">Type of the object.</param>
	/// <param name="myName">Name of the object.</param>
	/// <param name="mySustenance">Sustenance value of the object.</param>
	/// <param name="myFoodQuality">Food quality of the object.</param>
	/// <param name="amIHot">If set to <c>true</c> I am hot.</param>
	public Food(int myPrice, string myType, string myName, float mySustenance, float myFoodQuality, bool amIHot)
	{
		setPrice(myPrice);
		setType(myType);
		setName(myName);
		sustenanceMOD = mySustenance;
		foodQuality = myFoodQuality;
		hot = amIHot;
	}
	/// <summary>
	/// Adjusts the heat of the player.
	/// </summary>
	public void AdjustHeat()
	{
		if(hot)
		{
			Master.Instance.gameValues.setHeat(0.0035f);
		}
		else
		{
			Master.Instance.gameValues.setHeat (-0.025f);
		}
	}
	/// <summary>
	/// Adjusts the hygiene of the player.
	/// </summary>
	public void AdjustHygiene()
	{
		Master.Instance.gameValues.setHygiene(foodQuality);
	}
	/// <summary>
	/// Adjusts the sustenance value of the player.
	/// </summary>
	public void AdjustSustenance()
	{
		Master.Instance.gameValues.setSustenance(sustenanceMOD);
	}
	/// <summary>
	/// Called when the Item is used to initialize its properties.
	/// </summary>
	/// <param name="value">Value.</param>
	public override void Use(ref int value)
	{
		//initialise everything that need be done when drugs are used.
		if(value > 0)
		{
			value--;
			AdjustHeat();
			AdjustHygiene();
			AdjustSustenance();
		}
	}
}
