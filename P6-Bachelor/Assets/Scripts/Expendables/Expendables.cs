﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Core functionality for all expendable objects.
/// </summary>
public class Expendables : MonoBehaviour {

	#region variables
	/// <summary>
	/// The price of the expendable.
	/// </summary>
	private int price;
	/// <summary>
	/// The type of the expendable.
	/// </summary>
	private string type;
	/// <summary>
	/// The name of the expendable.
	/// </summary>
	private string name;
	#endregion variables
	#region getters
	/// <summary>
	/// Gets the price of the expendable.
	/// </summary>
	public int getPrice()
	{
		return price;
	}
	/// <summary>
	/// Gets the type of the expendable.
	/// </summary>
	public string getTypeExpend()
	{
		return type;
	}
	/// <summary>
	/// Gets the name of the expendable.
	/// </summary>
	public string getNameExpend()
	{
		return name;
	}
	#endregion getters
	#region setters
	/// <summary>
	/// Sets the price of the expendable.
	/// </summary>
	public void setPrice(int value)
	{
		price = value;
	}
	/// <summary>
	/// sets the type of the expendable.
	/// </summary>
	public void setType(string value)
	{
		type = value;
	}
	/// <summary>
	/// sets the name of the expendable.
	/// </summary>
	public void setName(string value)
	{
		name = value;
	}
	#endregion setters
	/// <summary>
	/// Sets up the values for the basic class.
	/// </summary>
	/// <param name="pValue">price</param>
	/// <param name="tName">type</param>
	/// <param name="nName">name</param>
	public Expendables()
	{
		price = 9999;
		type = "No Type, cuz this is standard expendable super class shizzness - No C allowed";
		name = "No Name, cuz this is standard expendable suer class shizznesss - No C allowed";
	}
	/// <summary>
	/// Called when the Item is used to initialize its properties.
	/// </summary>
	public virtual void Use(ref int value)
	{
		// For now this function does nothing - overrides will probably handle this.
	}
}
