﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Core functionality for all utility objects.
/// </summary>
public class Utilities : Expendables {
	/// <summary>
	/// The heat modifier, set only between -0.3 to 0.3;
	/// </summary>
	private float heatModifier;

	/// <summary>
	/// The hygiene modifier, set only between -5 and 5.
	/// </summary>
	private float hygieneModifier;

	/// <summary>
	/// Initializes a new instance of the <see cref="Utilities"/> class.
	/// </summary>
	/// <param name="myPrice">Price of the object.</param>
	/// <param name="myType">Type of the object.</param>
	/// <param name="myName">Name of the object.</param>
	/// <param name="myHeatMod">Heat modifier of the object.</param>
	/// <param name="myHygieneMod">Hygiene modifier of the object.</param>
	public Utilities(int myPrice, string myType, string myName, float myHeatMod, float myHygieneMod)
	{
		setPrice(myPrice);
		setType(myType);
		setName(myName);
		heatModifier = myHeatMod;
		hygieneModifier = myHygieneMod;
		heatModifier = Mathf.Clamp(heatModifier, -0.3f,0.3f);
		hygieneModifier = Mathf.Clamp(hygieneModifier, -0.2f,0.2f);
	}

	/// <summary>
	/// Gets the hygiene modifier.
	/// </summary>
	public float getHygieneMod()
	{
		return hygieneModifier;
	}

	/// <summary>
	/// Gets the heat modifier.
	/// </summary>
	public float getHeatMod()
	{
		return heatModifier;
	}

	/// <summary>
	/// Called when the Item is used to initialize its properties.
	/// </summary>
	/// <param name="value">Value.</param>
	public override void Use (ref int value)
	{
		if(value-- >0)
		{
			Master.Instance.gameValues.setHeat(heatModifier);
			Master.Instance.gameValues.setHygiene(hygieneModifier);
		}
	}

}
