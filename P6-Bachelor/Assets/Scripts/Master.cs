﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

/// <summary>
/// The master class, handles the entire system. If there is anything that multiple scripts need access to
/// it will be found here. Exceptions can occur, Camera is an easy call. And other singletons will have their own variables.
/// </summary>
public class Master : MonoBehaviour 
{
	/// <summary>
	/// If this is true, the player will be frozen.
	/// </summary>
	public bool inUI;
	/// <summary>
	/// When this goes true, the game ends.
	/// </summary>
	public bool endGame;
	/// <summary>
	/// Determines the speed of the screen fade system
	/// </summary>
	public float fadeSpeed = 1.5f; 
	/// <summary>
	/// Determines the speed of the Day and night cycle
	/// </summary>
	public float dayTimeCycle;
	/// <summary>
	/// If true it's day, if false, it's night.
	/// </summary>
	public bool day;
	/// <summary>
	/// Creates an Instance of the Master class. 
	/// This is done to make it a singleton. 
	/// Meaning only one instance, and the entire code can see it.
	/// </summary>
	public static Master Instance;
	/// <summary>
	/// refer to this evertime you want something to do with drugs.
	/// </summary>
	public Intoxicants drugs;
	/// <summary>
	/// refer to this evertime you want something to do with alcohol.
	/// </summary>
	public Intoxicants alcohol;
	/// <summary>
	/// refer to this evertime you want something to do with storebought food.
	/// </summary>
	public Food storebought;
	/// <summary>
	/// refer to this evertime you want something to do with food gained from trash.
	/// </summary>
	public Food trash;
	/// <summary>
	/// refer to this evertime you want something to do with shavers.
	/// </summary>
	public Utilities shaver;
	/// <summary>
	/// refer to this evertime you want something to do with toothbrushes.
	/// </summary>
	public Utilities toothbrush;
	/// <summary>
	/// refer to this evertime you want something to do with sleepingbags.
	/// </summary>
	public Utilities sleepingBag;
	/// <summary>
	/// refer to this evertime you want something to do with soap.
	/// </summary>
	public Utilities soap;
	/// <summary>
	/// refer to this evertime you want something to do with deodorant.
	/// </summary>
	public Utilities deo;
	/// <summary>
	/// If this is true, the screen will fade in and out again.based on the fadespeed.
	/// </summary>
	public bool FadeOut;
	/// <summary>
	/// The amount of money carried by the player.
	/// </summary>
	public int money;

	/// <summary>
	/// The amount of drugs on person.
	/// </summary>
	public int drugsOnPerson;
	/// <summary>
	/// The amount of alcohol on person.
	/// </summary>
	public int alcoOnPerson;
	/// <summary>
	/// The amount of storebought food on person.
	/// </summary>
	public int storeboughtFood;
	/// <summary>
	/// The amount of trash food on person.
	/// </summary>
	public int trashOnPerson;
	/// <summary>
	/// The amount of sleeping bags on person.
	/// </summary>
	public int sleepingBagOnPerson;
	/// <summary>
	/// The amount of toothbrushes on person.
	/// </summary>
	public int toothbrushOnPerson;
	/// <summary>
	/// The amount of shavers on person.
	/// </summary>
	public int shaverOnPerson;
	/// <summary>
	/// The amount of soap on person.
	/// </summary>
	public int soapOnPerson;
	/// <summary>
	/// The amount of deodorant on person.
	/// </summary>
	public int deoOnPerson;
	/// <summary>
	/// The amount of empty bottles on person.
	/// </summary>
	public int bottles;

	/// <summary>
	/// This function makes sure everything is initialized as it need be. 
	/// Also if you need to make alterations to any item values, do it here in their declarations.
	/// This function also makes sure there is only one master class in the scene.
	/// </summary>
	void Awake()
	{
			//Make sure there is only one Master class - If there is more, then the gameValues won't update properly.
		if(Instance != null)
		{
			Destroy (this);
		}
		else
		{
			Instance = this;
		}
			// change this for different values - Price, type of expendable, name, effect value, effect time. 
			//Effect time currently doesn't work due to system changes.
		alcohol = new Intoxicants(10, "Intoxicant", "Beer", 8, 20);
		drugs = new Intoxicants(25, "Intoxicant", "Doobie", 16, 10); // <-- In honour of Sune.
			// Foodz.
		storebought = new Food(20,"Food", "HotDog", 0.1f,0, true);
		trash = new Food(0,"Food", "Foraged", 0.07f, -0.05f,false);
			// Utilities
		shaver = new Utilities(10, "Utility", "Shaver", 0f,0.05f);
		toothbrush = new Utilities(10, "Utility", "Toothbrush",0f, 0.1f);
		sleepingBag = new Utilities(40,"Utility", "Sleeping Bag",0.1f, 0f);
		soap = new Utilities(10, "Utility", "Soap", 0f, 0.2f);
		deo = new Utilities(10, "Utility", "Deodorant", 0f, 0.05f);

		bottles = 0;
		day = true;
		inUI = false;
		endGame = false;
	}
	/// <summary>
	/// The core values. Every value relevant to the players condition/game's condition is stored in here.
	/// Refer to this if you need to alter anything about the character. Health, Bleakness, Hunger etc.
	/// </summary>
	public CoreValues gameValues = new CoreValues(0.3f, 0.2f, 0, 0, 1f);
		//Constructor.
    Master()
    {
    }
	/// <summary>
	/// Continually updates the corevalues, and the image saturation of the final render.
	/// </summary>
	public void Update()
	{
		if(!endGame)
		{
				// CoreValues class doesn't have MonoBehaviour so we call it's update function here.
			gameValues.UpdateMe();
		}
			// makes sure the program doesn't break if the shader doesn't work.
		if(Camera.main.GetComponent<ColorCorrectionCurves>().saturation != null)
		{
				//sets the saturation of the game = to Bleakness core value.
			Camera.main.GetComponent<ColorCorrectionCurves>().saturation = (gameValues.getBleakness()/100);
			//Debug.Log (Camera.main.GetComponent<ColorCorrectionCurves>().saturation);
		}
		else
		{
			Debug.LogWarning("There is no Saturation shader - this will break the game's main functions.");
		}
	}
}