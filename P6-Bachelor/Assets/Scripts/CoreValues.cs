﻿using UnityEngine;
using System.Collections;
/// <summary>
/// This function holds all the corevalues about the player and the game.
/// </summary>
public class CoreValues {
	#region Variables
	/// <summary>
	/// if it is day this will be true.
	/// </summary>
	private bool isItDay;
	/// <summary>
	/// whenever you sleep this is updated. Difference between this and current time defines whether or not you are tired.
	/// </summary>
	private float lastSlept;
	/// <summary>
	/// if this is true, the player is tired and needs to sleep.
	/// </summary>
	private bool tired;
	/// <summary>
	/// The game scales in dificulty based on this variable.
	/// </summary>
	private float scale;
	/// <summary>
	/// Defines whether or not a player has taken drugs.
	/// </summary>
	private bool hasPlayerTakenDrugs;
	/// <summary>
	/// The health, percentage calculation.
	/// </summary>
	private float health;
	/// <summary>
	/// The bleakness, percentage calculation.
	/// </summary>
	private float bleakness;
	/// <summary>
	/// Tmp bleakness, this allows for lerping between current and original values.
	/// </summary>
	private float tmpBleakness;
	/// <summary>
	/// The heat, can only be a value 
	/// between 0.1 and 0.3 - 0.3 is hot, 0.1 is cold.
	/// </summary>
	private float heat;
	/// <summary>
	/// The hygiene, can only be a value between -0.2 and 0.2 
	/// where -0.2 is really dirty and 0.2 is clean.
	/// </summary>
	private float hygiene;
	/// <summary>
	/// The toxicity value, used for calculating health. It is always 1/10th of the total drugUnits.
	/// Subtracted from the health, each point is an entire %.
	/// </summary>
	private float toxicity;
	/// <summary>
	/// The drug use, used for calculating bleakness.
	/// </summary>
	private float drugUse;
	/// <summary>
	/// The sustenance, can only be a value between 0 and 0.6 
	/// - where 0 is starved and 0.6 is fed.
	/// </summary>
	private float sustenance;
	/// <summary>
	/// The total amount of drug units throughout the game.
	/// </summary>
	private int drugUnits;
	/// <summary>
	/// The current drug units, reset if toxtime runs out.
	/// </summary>
	private int currDrugUnits;
	/// <summary>
	/// Total play time of the game.
	/// </summary>
	private float time;
	/// <summary>
	/// The detox time, the time it takes for the player to detox. 
	/// </summary>
	private float detoxTime;
	/// <summary>
	/// The detox time proxy, this will go up as detox time goes down.
	/// </summary>
	private float detoxTimeProxy;
	/// <summary>
	/// added to allow for longer and smoother intoxication.
	/// </summary>
	private float toxTime;
	/// <summary>
	/// The time scale value. The higher it is, the harder the game is to survive.
	/// Scales with time.
	/// </summary>
	private float timeScaleValue;
	/// <summary>
	/// The interaction chance.
	/// It is equal to the amount missing from health up to a hundred.
	/// If you have 98% health, you have 2% interaction value.
	/// if you have 54% health, you have 46% interaction value.
	/// </summary>
	private float interactionValue;
	/// <summary>
	/// The interaction Modifier. 
	/// Will modify certain variables based on how the player interacts with entities in the game.
	/// </summary>
	private float interactionMOD;
	/// <summary>
	/// Multiplier determines the frequency of hunger pings.
	/// </summary>
	private int multiplier;

	#endregion Variables
	/// <summary>
	/// Initializes a new instance of the <see cref="CoreValues"/> class. 
	/// This should take care of everything you need to consider. 
	/// If detoxTime or timeScaleValue is set to 0, it will automatically reset to 1.
	/// </summary>
	/// <param name="myHeat">My heat. Starting heat of the player.</param>
	/// <param name="myHygiene">My hygiene. Starting hygiene level of the player.</param>
	/// <param name="myDrugUnits">My drug units. Starting drug units of the player.</param>
	/// <param name="myTime">My time. Starting time (0)</param>
	/// <param name="myInteractionMod">My interaction mod. Starting interaction modifier.</param>
	/// <param name="myDetoxTime">My detox time. Starting detox time.</param>
	/// <param name="myTimeScaleValue">My time scale value. Starting time scale value (1)</param>
	public CoreValues (float myHeat, float myHygiene, int myDrugUnits, 
	                   float myInteractionMod, float myTimeScaleValue)
	{
		scale = 0;
		isItDay = false;
		lastSlept = 0f;
		tired = false;
		sustenance = 0.55f;
		heat = myHeat;
		hygiene = myHygiene;
		drugUnits = myDrugUnits;
		currDrugUnits = 4;
		time = 0f;
		interactionMOD = myInteractionMod;
		detoxTime = 60f;
		detoxTimeProxy = 1f;
		toxTime = 1f;
		hasPlayerTakenDrugs = true;
		drugUse = 0;
		toxicity = 0;
		multiplier = 1;
		if(myTimeScaleValue != 0f)
		{
			timeScaleValue = myTimeScaleValue;
		}
		else{
			timeScaleValue = 1f;
		}
			// updates the calculated values with the input values above.
		UpdateAllValues();
	}
	/// <summary>
	/// handles the updating up all the values based on a series of if statements.
	/// </summary>
	public void UpdateMe () {
		//Debug.Log ("CoreValues: "+time);
			//updates the time about once pr second, no matter how fast your computer is.
		if(time != Time.deltaTime)
		{
			time += Time.deltaTime;
				// every 10th second.
			//if(time >= multiplier*10)
			if(time >= multiplier)
			{
				if(time >= scale+90)
				{
					timeScaleValue += 0.5f;
					scale = time;
				}
				if(lastSlept+45f < (int)time)
				{
					// If the character sleeps this should happen. lastSlept = time;
					tired = true;
				}
					//Hunger Ping.
				calcSustenance();
				UpdateAllValues();
				multiplier++;
			}
				//If the player is on drugs, he will start to detox - this makes the detox value go down over time.
			if(hasPlayerTakenDrugs == true)
			{		//As long as detox time is more than the proxy then keep updating drugUse and Bleakness.
				//if(detoxTime > detoxTimeProxy)
				if(drugUse >= 0.1f)
				{
					if(toxTime > detoxTime)
					{
							//This will update the bleakness value - in turn updating the saturation of the game.
						detoxTimeProxy += (currDrugUnits/2)/100f; 
							//on my computer this takes aproximately 20 seconds after the detox starts.
					}
					else
					{
						toxTime += Time.deltaTime;
					}
						//always calculate druguse before Bleakness.
					calcDrugUse();
					calcBleakness();
				}	//when the player is no longer detoxing, reset the effects.
				else
				{
						//reset all values after detox period.
					UpdateAllValues();
					detoxTime = 0f;
					currDrugUnits = 0;
					toxTime = 1f;
					detoxTimeProxy = 1f;
					hasPlayerTakenDrugs = false;
				}
			}
		}
	}
//--------------------------------------------------------------------------------------------------------------------------
	#region Getters
	/// <summary>
	/// Gets the tired variable.
	/// </summary>
	/// <returns>the value of tired</returns>
	public bool getTired()
	{
		return tired;
	}
	/// <summary>
	/// Gets the has player taken drugs.
	/// </summary>
	/// <returns>whether or not the player is on drugs</returns>
	public bool getHasPlayerTakenDrugs()
	{
		return hasPlayerTakenDrugs;
	}
	/// <summary>
	/// Gets the tox time of the player.
	/// </summary>
	/// <returns>The tox time.</returns>
	public float getToxTime()
	{
		return toxTime;
	}

	/// <summary>
	/// Detox proxy is the comparative value to detoxTime.
	/// </summary>
	/// <returns>The detox proxy.</returns>
	public float getDetoxProxy()
	{
		return detoxTimeProxy;
	}

	/// <summary>
	/// Gets the health variable. Calculated from calcHealth function.
	/// </summary>
	/// <returns>The health.</returns>
	public float getHealth()
	{
		return health;
	}

	/// <summary>
	/// Gets the bleakness varialbe. Calculated from calcBleakness function.
	/// </summary>
	/// <returns>The bleakness.</returns>
	public float getBleakness()
	{
		return bleakness;
	}

	/// <summary>
	/// Gets the heat variable. Varies with player choices.
	/// </summary>
	/// <returns>The heat.</returns>
	public float getHeat()
	{
		return heat;
	}

	/// <summary>
	/// Gets the hygiene variable.Varies with player choices.
	/// </summary>
	/// <returns>The hygiene.</returns>
	public float getHygiene()
	{
		return hygiene;
	}

	/// <summary>
	/// Gets the toxicity variable. Is equal to 1/10th of total drugunits.
	/// </summary>
	/// <returns>The toxicity.</returns>
	public float getToxicity()
	{
		return toxicity;
	}

	/// <summary>
	/// Gets the drug use varible. Is calculated from calcDrugUse.
	/// </summary>
	/// <returns>The drug use.</returns>
	public float getDrugUse()
	{
		return drugUse;
	}

	/// <summary>
	/// Gets the sustenance variable. Is calculated from calcSustenance.
	/// </summary>
	/// <returns>The sustenance.</returns>
	public float getSustenance()
	{
		return sustenance;
	}
	/// <summary>
	/// Gets the drug units variable. Accumulates from continuous drug abuse.
	/// </summary>
	/// <returns>The drug units.</returns>
	public int getDrugUnits()
	{
		return drugUnits;
	}
	/// <summary>
	/// Gets the curr drug units variable. Accumulates from continuous drug abuse.
	/// This resets when the player has detoxed.
	/// </summary>
	/// <returns>The curr drug units.</returns>
	public int getCurrDrugUnits()
	{
		return currDrugUnits;
	}
	/// <summary>
	/// Gets the total detox time of the player.
	/// </summary>
	/// <returns>The detox time.</returns>
	public float getDetoxTime()
	{
		return detoxTime;
	}
	/// <summary>
	/// Gets the total play time of this session.
	/// </summary>
	/// <returns>The time.</returns>
	public float getTime()
	{
		return time;
	}
	/// <summary>
	/// Gets the time scale value.
	/// </summary>
	/// <returns>The time scale value.</returns>
	public float getTimeScaleValue()
	{
		return timeScaleValue;
	}
	/// <summary>
	/// Gets the interaction value.
	/// </summary>
	/// <returns>The interaction value.</returns>
	public float getInteractionValue()
	{
		return interactionValue;
	}
	/// <summary>
	/// Gets the interaction MOD.
	/// </summary>
	/// <returns>The interaction MOD.</returns>
	public float getInteractionMOD()
	{
		return interactionMOD;
	}

	#endregion Getters
//--------------------------------------------------------------------------------------------------------------------------
	#region Setters
	/// <summary>
	/// Resets the drug variables. 
	/// Call this anytime you need the player to come down from his high.
	/// </summary>
	public void resetDrugs()
	{
		detoxTime = 0f;
		currDrugUnits = 0;
		toxTime = 1f;
		detoxTimeProxy = 1f;
		hasPlayerTakenDrugs = false;
	}
	/// <summary>
	/// Sets the tired variable.
	/// Also updates the lastSlept variable.
	/// </summary>
	/// <param name="value">If set to <c>true</c> value.</param>
	public void setTired(bool value)
	{
		lastSlept = time;
		tired = value;
	}
	/// <summary>
	/// Sets whether or not the player has taken drugs.
	/// </summary>
	/// <param name="value">If set to <c>true</c> then the game will consider the player as on drugs.</param>
	public void setHasPlayerTakenDrugs(bool value)
	{
		 hasPlayerTakenDrugs = value;
	}
	/// <summary>
	/// Sets the heat variable based on player choices.
	/// </summary>
	/// <param name="value">Value.</param>
	public void setHeat(float value)
	{
		heat += value;
		heat = Mathf.Clamp (heat, 0.1f, 0.3f);
	}
	/// <summary>
	/// Sets the sustenance value.
	/// </summary>
	/// <param name="value">Value.</param>
	public void setSustenance(float value)
	{
		sustenance += value;
		sustenance = Mathf.Clamp(sustenance, 0f, 0.6f);
	}
	/// <summary>
	/// Sets the hygiene value.
	/// If you want to subtract make the value a negative number.
	/// </summary>
	/// <returns>The new hygiene value.</returns>
	/// <param name="value">value + hygiene = new value. </param>
	public void setHygiene(float value)
	{
		hygiene += value;
		hygiene = Mathf.Clamp (hygiene, -0.4f, 0.2f);
	}

	/// <summary>
	/// Sets the drug units value.
	/// If you want to subtract make the value a negative number.
	/// </summary>
	/// <returns>The drug units value.</returns>
	/// <param name="value">value + drugUnits = new value.</param>
	public void setDrugUnits(int value)
	{
		drugUnits += value;
	}

	/// <summary>
	/// Sets the current drug units.
	/// If you want to subtract make the value a negative number.
	/// </summary>
	/// <returns>The current drug units.</returns>
	/// <param name="value">value + currDrugUnits = new value. </param>
	public void setCurrDrugUnits(int value)
	{
		currDrugUnits += value;
	}

	/// <summary>
	/// Sets the detox time.
	/// If you want to subtract make the value a negative number.
	/// </summary>
	/// <returns>The detox time.</returns>
	/// <param name="value">value + detoxTime = new value. </param>
	public void setDetoxTime(int value)
	{
		detoxTime += value;
	}

	/// <summary>
	/// Sets the time scale value.
	/// If you want to subtract make the value a negative number.
	/// </summary>
	/// <returns>The time scale value.</returns>
	/// <param name="value">value + timeScaleValue = new value.</param>
	public void setTimeScaleValue(float value)
	{
		timeScaleValue = 0;
		timeScaleValue += value;
	}
	/// <summary>
	/// Sets the detox proxy value to 1.
	/// </summary>
	public void setDetoxProxy()
	{
		detoxTimeProxy = 1f;
	}

	#endregion Setters
//--------------------------------------------------------------------------------------------------------------------------
	#region Calculators
	/// <summary>
	/// Calculates the health variable.
	/// health = ((sustenance+heat+(hygiene/2))*100)-toxicity;
	/// Clamped from 1 to 100.
	/// </summary>
	public void calcHealth()
	{
		health = ((sustenance+heat+(hygiene/2))*100)-toxicity;
		health = Mathf.Clamp(health, 1f, 100f);
	}

	/// <summary>
	/// Calculates the bleakness variable.
	/// bleakness = ((drugUse/10+(sustenance/2)+interactionMOD)/timeScaleValue)*100;
	/// Clamped from 1 to 100.
	/// </summary>
	public void calcBleakness()
	{
		bleakness = ((drugUse/10+(sustenance/2)+interactionMOD)/timeScaleValue)*100;
		bleakness = Mathf.Clamp(bleakness, 1f,100f);
	}

	/// <summary>
	/// Calculates the sustenance variable.
	/// sustenance -= (sustenance/75); Removes .75% of the total value each time it's called.
	/// Clamped between 0 & 0.6
	/// </summary>
	public void calcSustenance()
	{
		sustenance -= (sustenance/75);
		sustenance = Mathf.Clamp(sustenance, 0f, 0.6f);
	}

	/// <summary>
	/// Calculates the drug use variable.
	/// drugUse = currDrugUnits/detoxTimeProxy;
	/// </summary>
	public void calcDrugUse()
	{
		drugUse = currDrugUnits/detoxTimeProxy;
	}

	/// <summary>
	/// Calculates the toxicity variable.
	/// toxicity = drugUnits/10;
	/// </summary>
	public void calcToxicity()
	{
		toxicity = drugUnits/10;
	}

	/// <summary>
	/// Calculates the interaction value variable.
	/// interactionValue = ((health-100)+toxicity)*(-1);
	/// Clamped between 1 & 100.
	/// </summary>
	public void calcInteractionValue()
	{
		interactionValue = ((health-100)+toxicity)*(-1);
		interactionValue = Mathf.Clamp(interactionValue, 1f, 100f);
	}

	/// <summary>
	/// Updates all calculated values.
	/// </summary>
	public void UpdateAllValues()
	{
		calcDrugUse();
		calcToxicity();
		calcInteractionValue();
		calcHealth();
		calcBleakness();
	}
	#endregion Calculators
}
