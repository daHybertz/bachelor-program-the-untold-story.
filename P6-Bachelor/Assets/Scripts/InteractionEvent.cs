using UnityEngine;
using System.Collections;
/// <summary>
/// Player input function. Handles all the user input to the player functions.
/// </summary>
public class InteractionEvent : MonoBehaviour {

	/// <summary>
	/// Determines whether or not an event is active.
	/// </summary>
	bool activateEvent;
	/// <summary>
	/// if the player is moving this variable is true.
	/// </summary>
	public bool isPlayerMoving;
	/// <summary>
	/// The zoom of the camera. 
	/// The larger this becomes, the more is visible on the camera.
	/// </summary>
	float camSize;
	/// <summary>
	/// The movespeed of the player..
	/// </summary>
	public float moveSpeed;
	/// <summary>
	/// The cameras movement speed.
	/// </summary>
	public float camFloatSpeed;
	/// <summary>
	/// If the player can interact with something. 
	/// This is true, and this sets a UI element for feedback.
	/// </summary>
	public bool feedbackInteraction;
	/// <summary>
	/// Stores the time the player moved.
	/// Is used for smoothing of camera handling.
	/// </summary>
	float timeIMoved;
	/// <summary>
	/// Set in the inspector. 
	/// Is the amount of time the camera waits before it moves & zooms.
	/// </summary>
	public float timeToMove;
	/// <summary>
	/// Holds the animation data from the side animations.
	/// </summary>
	public SkeletonDataAsset side;
	/// <summary>
	/// Holds the front animation data.
	/// </summary>
	public SkeletonDataAsset front;
	/// <summary>
	/// Holds the back animation data.
	/// </summary>
	public SkeletonDataAsset back;

	/// <summary>
	/// If the player is moving right, this is true, if he is moving left this is false.
	/// If this is false and the player moves right, the player animations will flip from left to right.
	/// </summary>
	public bool facingRight;
	//helps with the animations.
	bool a;
	bool d;
	bool w;
	bool s;

	/// <summary>
	/// If the player hasn't been at the kiosk, 
	/// output tutorial text to the output bar on the screen.
	/// </summary>
	bool hasBeenAtKiosk;
	/// <summary>
	/// If the player hasn't been at the dealer, 
	/// output tutorial text to the output bar on the screen.
	/// </summary>
	bool hasBeenAtDealer;
	/// <summary>
	/// If the player hasn't been at the mall, 
	/// output tutorial text to the output bar on the screen.
	/// </summary>
	bool hasBeenAtMall;
	/// <summary>
	/// If the player hasn't been at a trash can, 
	/// output tutorial text to the output bar on the screen.
	/// </summary>
	bool hasBeenAtTrash;
	/// <summary>
	/// If the player hasn't been at the toilet, 
	/// output tutorial text to the output bar on the screen.
	/// </summary>
	bool hasBeenAtToilet;
	/// <summary>
	/// If the player hasn't been at an NPC, 
	/// output tutorial text to the output bar on the screen.
	/// </summary>
	bool hasBeenAtNPC;
	/// <summary>
	/// If the player hasn't been at the homeless shelter, 
	/// output tutorial text to the output bar on the screen.
	/// </summary>
	bool hasBeenAtShelter;
	/// <summary>
	/// If the player hasn't been at the heatinglounge, 
	/// output tutorial text to the output bar on the screen.
	/// </summary>
	bool hasBeenAtHeatingLounge;

	/// <summary>
	/// Initilizes the variables needed for this script.
	/// </summary>
	void Start () {
		facingRight = true;
		a = false;
		d = false;
		w = false;
		s = false;

		hasBeenAtKiosk = false;
		hasBeenAtDealer = false;
		hasBeenAtMall = false;
		hasBeenAtTrash = false;
		hasBeenAtToilet = false;
		hasBeenAtNPC = false;
		hasBeenAtShelter = false;
		hasBeenAtHeatingLounge = false;


		activateEvent = false;
		isPlayerMoving = false;
		camSize = Camera.main.GetComponent<Camera>().orthographicSize = 1.5f;
	}
	
	/// <summary>
	/// Checks for input and reacts accordingly.
	/// </summary>
	void Update () {
		if(Input.GetKeyDown(KeyCode.A) && !isPlayerMoving || Input.GetKeyDown(KeyCode.D) && !isPlayerMoving || 
		   Input.GetKeyDown(KeyCode.W) && !isPlayerMoving || Input.GetKeyDown(KeyCode.S) && !isPlayerMoving)
		{
			timeIMoved = Master.Instance.gameValues.getTime();
		}
		if(Input.GetKey(KeyCode.A) && !Master.Instance.inUI || Input.GetKey(KeyCode.D) && !Master.Instance.inUI
		   || Input.GetKey(KeyCode.W) && !Master.Instance.inUI || Input.GetKey(KeyCode.S) && !Master.Instance.inUI)
		{
			if(Input.GetKey(KeyCode.A))
			{
				transform.position += new Vector3(-moveSpeed*Time.deltaTime, 0,0);
				if(facingRight)
				{
					Flip();
				}

				if(Master.Instance.gameValues.getHeat() < 0.2f && !a)
				{
					GetComponent<SkeletonAnimation>().skeletonDataAsset = side;
					GetComponent<SkeletonAnimation>().Reset();
					GetComponent<SkeletonAnimation>().state.SetAnimation(2,"walk_cold",true);
					GetComponent<SkeletonAnimation>().timeScale = 1f;
					a = true;
				}
				else if(!a)
				{

					GetComponent<SkeletonAnimation>().skeletonDataAsset = side;
					GetComponent<SkeletonAnimation>().Reset();
					GetComponent<SkeletonAnimation>().state.SetAnimation(1,"walk",true);
					GetComponent<SkeletonAnimation>().timeScale = 1f;
					a = true;
				}
			}
			if(Input.GetKey(KeyCode.D))
			{
				transform.position += new Vector3(moveSpeed*Time.deltaTime, 0,0);
				if(!facingRight)
				{
					Flip();
				}

				if(Master.Instance.gameValues.getHeat() < 0.2f && !d)
				{
					GetComponent<SkeletonAnimation>().skeletonDataAsset = side;
					GetComponent<SkeletonAnimation>().Reset();
					GetComponent<SkeletonAnimation>().state.SetAnimation(2,"walk_cold",true);
					GetComponent<SkeletonAnimation>().timeScale = 1f;
					d = true;
				}
				else if(!d)
				{
					GetComponent<SkeletonAnimation>().skeletonDataAsset = side;
					GetComponent<SkeletonAnimation>().Reset();
					GetComponent<SkeletonAnimation>().state.SetAnimation(1,"walk",true);
					GetComponent<SkeletonAnimation>().timeScale = 1f;
					d = true;
				}
			}
			if(Input.GetKey(KeyCode.W))
			{
				if(Master.Instance.gameValues.getHeat() < 0.2f && !w)
				{
					GetComponent<SkeletonAnimation>().skeletonDataAsset = back;
					GetComponent<SkeletonAnimation>().Reset();
					GetComponent<SkeletonAnimation>().state.SetAnimation(2,"walk_cold",true);
					GetComponent<SkeletonAnimation>().timeScale = 1.5f;
					w = true;
				}
				else if(!w)
				{
					w = true;
					GetComponent<SkeletonAnimation>().skeletonDataAsset = back;
					GetComponent<SkeletonAnimation>().Reset();
					GetComponent<SkeletonAnimation>().state.SetAnimation(1,"walk",true);
					GetComponent<SkeletonAnimation>().timeScale = 1.5f;
				}
				transform.position += new Vector3(0, moveSpeed*Time.deltaTime,0);
			}
			if(Input.GetKey(KeyCode.S))
			{

				if(Master.Instance.gameValues.getHeat() < 0.2f && !s)
				{
					GetComponent<SkeletonAnimation>().skeletonDataAsset = front;
					GetComponent<SkeletonAnimation>().Reset();
					GetComponent<SkeletonAnimation>().state.SetAnimation(2,"walk_cold",true);
					GetComponent<SkeletonAnimation>().timeScale = 1.5f;
					s = true;
				}
				else if(!s)
				{
					GetComponent<SkeletonAnimation>().skeletonDataAsset = front;
					GetComponent<SkeletonAnimation>().Reset();
					GetComponent<SkeletonAnimation>().state.SetAnimation(1,"walk",true);
					GetComponent<SkeletonAnimation>().timeScale = 1.5f;
					s = true;
				}
				transform.position += new Vector3(0, -moveSpeed*Time.deltaTime,0);
			}
			isPlayerMoving = true;
		}
		else
		{
			isPlayerMoving = false;
		}
		if(Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.D))
		{
			if(Master.Instance.gameValues.getHeat() < 0.2f)
			{
				GetComponent<SkeletonAnimation>().skeletonDataAsset = front;
				GetComponent<SkeletonAnimation>().Reset();
				GetComponent<SkeletonAnimation>().state.SetAnimation(1,"walk_cold",false);
				GetComponent<SkeletonAnimation>().timeScale = 1f;
			}
			else
			{
				GetComponent<SkeletonAnimation>().skeletonDataAsset = front;
				GetComponent<SkeletonAnimation>().Reset();
				GetComponent<SkeletonAnimation>().state.SetAnimation(1,"walk",false);
				GetComponent<SkeletonAnimation>().timeScale = 1f;
			}


			a = false;
			d = false;
			w = false;
			s = false;
		}

		if(isPlayerMoving && timeIMoved+timeToMove < Master.Instance.gameValues.getTime () && !Master.Instance.inUI)
		{
			camSize = Mathf.Lerp(camSize, 2.5f, (camFloatSpeed/1.5f)*Time.deltaTime);
			Camera.main.GetComponent<Camera>().orthographicSize = camSize;
		}
		else
		{
			if(Camera.main.GetComponent<CamSystem>().canMoveHorizontal 
			   && Camera.main.GetComponent<CamSystem>().canMoveVertical && !Master.Instance.inUI)
			{
			camSize = Mathf.Lerp(camSize, 1.5f, camFloatSpeed*Time.deltaTime);
			Camera.main.GetComponent<Camera>().orthographicSize = camSize;
			}
		}
	}
	/// <summary>
	/// unity inbuilt  OnTriggerEnter2D event.
	/// when the players collider enters a 2D trigger it's functionality will run.
	/// </summary>
	/// <param name="other">Other.</param>
	void OnTriggerEnter2D(Collider2D other)
	{
		string h = other.gameObject.name;
		switch (h)
		{
			case "Dealer": 
				if(!hasBeenAtDealer)
				{
				OutputText.Instance.NewMessage("\nThis man is a drug dealer, he sells drugs. He gives you a free sample <B> Press E to open the shop to buy more.</B> \n","#00FF00");
					hasBeenAtDealer = true;
					Master.Instance.drugsOnPerson++;
				}
			break;
			case "Mall": 
				if(!hasBeenAtMall)
				{
				OutputText.Instance.NewMessage("\nThis is the Mall they sell general supplies that will help you survive the game. <b>Press E to enter the shop </b> \n","#00FF00");
					hasBeenAtMall = true;
				}
			break;
			case "Kiosk": 
				if(!hasBeenAtKiosk)
				{
				OutputText.Instance.NewMessage("\nThis is the kiosk, they sell general supplies that will help you survive the game. <b>Press E to enter the shop </b> \n","#00FF00");
					hasBeenAtKiosk = true;
				}
			break;
			case "Trash": 
				if(!hasBeenAtTrash)
				{
				OutputText.Instance.NewMessage("\nThis is a trash can, you may find valuables or spare food in here. Once it has been emptied you will have to wait until it fills up again. <b>Press E to see if there is something in there.</b> \n", "#00FF00");
					hasBeenAtTrash = true;
				}
			break;
			case "NPC": 
				if(!hasBeenAtNPC)
				{
				OutputText.Instance.NewMessage("\nThis is an ordinary person, you may be able to beg some money out of him. <b>Press E to beg.</b>\n","#00FF00");
					hasBeenAtNPC = true;
				}
			break;
			case "Shelter": 
			if(!hasBeenAtShelter)
			{
				OutputText.Instance.NewMessage("\nHomeless Shelters give you food, something to eat and a place to clean yourself. They are pretty overburdened though so they may not always have room. <b> Press E to request assistance</b> \n","#00FF00");
				hasBeenAtShelter = true;
			}
			break;
			case "HeatingLounge": 
			if(!hasBeenAtHeatingLounge)
			{
				OutputText.Instance.NewMessage("\nIn Denmark homeless people have the opportunity to go to heating lounges and sit to heat up and sometimes get something to eat. They are pretty underfunded so they may not always have the funds to help you. <b> Press E to request Assistance.</b> \n","#00FF00");
				hasBeenAtHeatingLounge = true;
			}
			break;
			case "Toilet": 
			if(!hasBeenAtToilet)
			{
				OutputText.Instance.NewMessage("\nLiving on the street can get pretty messy, and lead to all kinds of diseases, infections and whatnaught. If you have the proper equipment you can clean yourself up here. <b>Press E to clean yourself up.</b> \n","#00FF00");
				hasBeenAtToilet = true;
			}
			break;

		}
	}
	/// <summary>
	/// unity inbuilt  OnTriggerStay2D event.
	/// when the players collider stays inside a 2D trigger it's functionality will run.
	/// </summary>
	/// <param name="other">Other.</param>
	void OnTriggerStay2D(Collider2D other){
		if(other.tag == "Interactible")
		{
			feedbackInteraction = true;
		}
		if(other.tag == "Interactible" && !activateEvent){
			if(Input.GetKeyDown(KeyCode.E))
			{
				activateEvent = true;
				other.gameObject.GetComponent<Interactibles>().ActivateMyEvent();
			}
		}
		if(other.tag == "LeftRight"){
			Camera.main.GetComponent<CamSystem>().canMoveHorizontal = false;
		}
		if(other.tag == "TopBottom")
		{
			Camera.main.GetComponent<CamSystem>().canMoveVertical = false;
		}
	}
	/// <summary>
	/// unity inbuilt  OnTriggerExit2D event.
	/// when the players collider exits a 2D trigger it's functionality will run.
	/// </summary>
	/// <param name="other">Other.</param>
	void OnTriggerExit2D(Collider2D other)
	{
		if(other.tag == "Interactible")
		{
			feedbackInteraction = false;
		}
		if(other.tag == "Interactible" && activateEvent)
		{
			activateEvent = false;
		}
		if(other.tag == "LeftRight")
		{
			Camera.main.GetComponent<CamSystem>().canMoveHorizontal = true;
		}
		if(other.tag == "TopBottom")
		{
			Camera.main.GetComponent<CamSystem>().canMoveVertical = true;
		}
	}
	/// <summary>
	/// Flips the animations from left to right.
	/// </summary>
	void Flip()
	{
		// Switch the way the player is labelled as facing
		facingRight = !facingRight;
		
		// Multiply the player's x local scale by -1
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
	
}
