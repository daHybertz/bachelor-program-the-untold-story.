﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
/// <summary>
/// Handles all the drug dealer UI funcitonality.
/// </summary>
public class BuyFromDealer : MonoBehaviour {
	/// <summary>
	/// Shows the cost of drugs from the drugdealer
	/// </summary>
	Text t;
	/// <summary>
	/// displays the money in the players pocket.
	/// </summary>
	public Text moneyInPocket;
	/// <summary>
	/// Contains the reference to the player.
	/// </summary>
	public GameObject p;

	/// <summary>
	/// Handles initialization of everything.
	/// </summary>
	public void Start()
	{
		t = transform.FindChild("Text").GetComponent<Text>();
		t.text = "<b> Costs: "+Master.Instance.drugs.getPrice()+" Euros</b>";
	}
	/// <summary>
	/// Continually updates the money related text variable.
	/// </summary>
	public void Update()
	{
		moneyInPocket.text = "Money in pocket: "+Master.Instance.money;
	}
	/// <summary>
	/// When button is pushed, this buys the drugs in question.
	/// Also spawns a neat little plus to give visual feedback.
	/// </summary>
	public void OnBuyDrugs()
	{
		float r = Random.Range(-50,50);
		r = r/100;
		if(Master.Instance.drugs.getPrice() <= Master.Instance.money)
		{
			GameObject a =
			Instantiate(p, new Vector3(t.gameObject.transform.position.x+r,t.gameObject.transform.position.y,t.gameObject.transform.position.z) ,Quaternion.identity)
					as GameObject;
			transform.parent.FindChild("ExitBtn").GetComponent<ExitShop>().pp.Add(a);
			Master.Instance.money -= Master.Instance.drugs.getPrice();
			Master.Instance.drugsOnPerson++;

		}
	}
}
