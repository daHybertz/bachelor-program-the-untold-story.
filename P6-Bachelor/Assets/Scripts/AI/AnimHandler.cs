using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// Handles the NPC Animations.
/// </summary>
public class AnimHandler : MonoBehaviour {

	/// <summary>
	/// This list allow us to continously update target spriteRenderer, and create animations with code.
	/// </summary>
	public List<Sprite> defList = new List<Sprite>();
	/// <summary>
	/// The list allow us to continously update target spriteRenderer, and create animations with code.
	/// </summary>
	public List<Sprite> defListR = new List<Sprite>();
	/// <summary>
	///These bools help define what animation to start.
	/// </summary>
	public bool rightBool; 
	/// <summary>
	///These bools help define what animation to start.
	/// </summary>
	public bool leftBool;
	/// <summary>
	///These bools help define what animation to start.
	/// </summary>
	public bool canShy;
	/// <summary>
	///These bools help define what animation to start.
	/// </summary>
	public bool leftShyAway;

	/// <summary>
	/// Stores a sprite for the animation. 
	/// Is made public so it can be inserted from the editor into the inspector.	
	/// </summary>
	public Sprite walk1;
	/// <summary>
	/// Stores a sprite for the animation. 
	/// Is made public so it can be inserted from the editor into the inspector.	
	/// </summary>
	public Sprite walk2;
	/// <summary>
	/// Stores a sprite for the animation. 
	/// Is made public so it can be inserted from the editor into the inspector.	
	/// </summary>
	public Sprite walk3;
	/// <summary>
	/// Stores a sprite for the animation. 
	/// Is made public so it can be inserted from the editor into the inspector.	
	/// </summary>
	public Sprite walk4;
	/// <summary>
	/// Stores a sprite for the animation. 
	/// Is made public so it can be inserted from the editor into the inspector.	
	/// </summary>
	public Sprite walk5;
	/// <summary>
	/// Stores a sprite for the animation. 
	/// Is made public so it can be inserted from the editor into the inspector.	
	/// </summary>
	public Sprite walk1R;
	/// <summary>
	/// Stores a sprite for the animation. 
	/// Is made public so it can be inserted from the editor into the inspector.	
	/// </summary>
	public Sprite walk2R;
	/// <summary>
	/// Stores a sprite for the animation. 
	/// Is made public so it can be inserted from the editor into the inspector.	
	/// </summary>
	public Sprite walk3R;
	/// <summary>
	/// Stores a sprite for the animation. 
	/// Is made public so it can be inserted from the editor into the inspector.	
	/// </summary>
	public Sprite walk4R;
	/// <summary>
	/// Stores a sprite for the animation. 
	/// Is made public so it can be inserted from the editor into the inspector.	
	/// </summary>
	public Sprite walk5R;


	/// <summary>
	/// Initializes the variables needed for this script.
	/// </summary>
	void Start () 
	{
		//rightBool = false; // These bools help define what animation to start.
		//leftBool = false;
		canShy = false;

		//Default attack animation to the right
		/*addSpritesToList(defAtkList, defAttack1, 7); // add to list takes in a list, a sprite and a number of iterations.
		addSpritesToList(defAtkList, defAttack2, 7); // it basically fills up the given list with sprites as designated.
		addSpritesToList(defAtkList, defAttack1, 7);
		addSpritesToList(defAtkList, defAttack2, 7);
		defAtkList.Add (walk1);
		defAtkList.Add (walk1);*/

		//Default attack animation to the left
		/*addSpritesToList(defAtkListR, defAttack1R, 7);
		addSpritesToList(defAtkListR, defAttack2R, 7);
		addSpritesToList(defAtkListR, defAttack1R, 7);
		addSpritesToList(defAtkListR, defAttack2R, 7);
		defAtkListR.Add (walk1R);
		defAtkListR.Add (walk1R);*/

		//Movements

		addSpritesToList(defList, walk1, 6);
		addSpritesToList(defListR, walk1R, 6);

		addSpritesToList(defList, walk2, 6);
		addSpritesToList(defListR, walk2R, 6);
		
		addSpritesToList(defList, walk3, 6);
		addSpritesToList(defListR, walk3R, 6);

		addSpritesToList(defList, walk4, 6);
		addSpritesToList(defListR, walk4R, 6);

		addSpritesToList(defList, walk5, 6);
		addSpritesToList(defListR, walk5R, 6);
	}
	/// <summary>
	/// Automatically adds the sprites to each list.
	/// </summary>
	/// <param name="tmpList">Tmp list. references the list in question.</param>
	/// <param name="tmpSprite">Tmp sprite. references the sprite in question</param>
	/// <param name="p">P. references the amount of iterations, meaning the amount of sprites to add to the list.</param>
	void addSpritesToList(List<Sprite> tmpList, Sprite tmpSprite, int p) // fills up the lists, more reusable code compared to the hardcoded version we had before.
	{
		for(int i = 0; i<p; i++)
			tmpList.Add(tmpSprite);
	}
	/// <summary>
	/// The q. defines where in the list we are, and what sprite to load.
	/// </summary>
	public int q = 0; 
	/// <summary>
	/// Updates the spriterenderer.
	/// </summary>
	void Update () 
	{
		/*if(canShy == true) // you can start the attack animation
		{
			if(leftShyAway == true) // attack left
			{
				//Attack to the left animations for the specific body
				if(GetComponent<player>().currBody.name == "Default")
					GetComponent<SpriteRenderer>().sprite = defAtkListR[q];


			}
			if(leftShyAway == false) // attack right.
			{
				//Attack to the right animations for the specific body
				if(GetComponent<player>().currBody.name == "Default")
					GetComponent<SpriteRenderer>().sprite = defAtkList[q];
			}
		}*/
		if(rightBool == true && canShy != true) // if true look for current body var in player.cs and run the appropriate animation.
		{
				GetComponent<SpriteRenderer>().sprite = defList[q];
		}
		if(leftBool == true && canShy != true) // same as above but with inversed images.
		{
				GetComponent<SpriteRenderer>().sprite = defListR[q];
		}
		if(q >= 29) // make sure the q doesn't go out of list's boundries.
		{
			canShy = false; // so attack cycle will only run once.
			q = 0;
		}
		q++; // increment to next step.
	}
	/// <summary>
	/// flips the sprites.
	/// </summary>
	public void TurnAround()
	{
		leftBool = !leftBool;
		rightBool = !rightBool;
	}
}
