﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Handles the NPC walking back and forth between triggers.
/// </summary>
public class AIWalk : MonoBehaviour {
	/// <summary>
	/// If the player has been at the first trigger.
	/// it goes to the second. 
	/// If it hasn't it goes to the first.	/// </summary>
	public bool hasBeenAtOne;
	/// <summary>
	/// The movementspeed of the npc.
	/// </summary>
	public float speed = 0.1f;
	
	/// <summary>
	///updates the animations and the movements of the NPC's.
	/// </summary>
	void Update () {
		if(!hasBeenAtOne)
		{
			this.transform.position = new Vector3(transform.position.x +speed*Time.deltaTime, transform.position.y, transform.position.z);
			if(GetComponent<AnimHandler>().leftBool)
			{
				GetComponent<AnimHandler>().TurnAround();
			}
		}
		else
		{
			this.transform.position = new Vector3(transform.position.x -speed*Time.deltaTime, transform.position.y, transform.position.z);
			if(GetComponent<AnimHandler>().rightBool)
			{
				GetComponent<AnimHandler>().TurnAround();
			}
		}
		
	}
	/// <summary>
	/// unity inbuilt  OnTriggerEnter2D event.
	/// when the NPCs collider enters a 2D trigger it's functionality will run.
	/// </summary>
	/// <param name="other">Other.</param>
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject.tag == "AIPath")
		{
			if(!hasBeenAtOne){
				hasBeenAtOne = true;
			}
			else {
				hasBeenAtOne = false;
			}
		}
	}
}
