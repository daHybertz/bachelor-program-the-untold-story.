﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Handles the functionality of the public toilet.
/// </summary>
public class PublicToilet : Interactibles {
	/// <summary>
	/// A variable that keeps track of the totalt uses throughout the game.
	/// </summary>
	int uses;
	/// <summary>
	/// max uses that the toilet has throughout the game.
	/// </summary>
	public int runOutOfUses;
	/// <summary>
	/// Initializes the variables needed for this script.
	/// </summary>
	public override void Start ()
	{	type = "Toilet";
		name = type;
		uses = 0;
	}
	/// <summary>
	/// Activates the event of this object.
	/// </summary>
	public override void ActivateMyEvent()
	{
		if(uses < runOutOfUses)
		{
			if(Master.Instance.shaverOnPerson > 0)
			{
				Master.Instance.shaver.Use(ref Master.Instance.shaverOnPerson);
			}
			if(Master.Instance.deoOnPerson > 0)
			{
				Master.Instance.deo.Use (ref Master.Instance.deoOnPerson);
			}
			if(Master.Instance.soapOnPerson > 0)
			{
				Master.Instance.soap.Use(ref Master.Instance.soapOnPerson);
			}
			if(Master.Instance.toothbrushOnPerson > 0)
			{
				Master.Instance.toothbrush.Use(ref Master.Instance.toothbrushOnPerson);
			}
			uses++;
			Master.Instance.fadeSpeed  = 3f;
			Master.Instance.FadeOut = true;
			OutputText.Instance.NewMessage("You feel refreshed from a nice wash", "#00FFFF");
		}
		else
		{
			OutputText.Instance.NewMessage("This toilet looks like it hasn't been cleaned in a month. " +
			                               "You would probably get more dirty than clean from spending time in here.", "#B4D400");
		}
	}

}
