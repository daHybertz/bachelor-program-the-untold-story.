﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Core functionality for the interactibles.
/// </summary>
public class Interactibles : MonoBehaviour {
	/// <summary>
	/// The type of interactible.
	/// </summary>
	public string type;
	/// <summary>
	/// Holds the day and night cycle (Relevant for the places to sleep)
	/// </summary>
	public GameObject DaNCycle;
	/// <summary>
	/// Initializes the variables needed for this script.
	/// </summary>
	public virtual void Start () {
		type = "no type";
		DaNCycle = GameObject.FindGameObjectWithTag("DANCycle");
	}
	
	// Update is called once per frame
	public virtual void Update () {
	}

	/// <summary>
	/// Activates the event of this object.
	/// </summary>
	public virtual void ActivateMyEvent()
	{
		//Debug.Log("Activating Event: " + gameObject.name);
	}
}
