﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Handles the shops non-UI interaction functionality.
/// </summary>
public class Shop : Interactibles {
	/// <summary>
	/// Holds a reference to the dealer object.
	/// </summary>
	public GameObject dealer;
	/// <summary>
	/// Holds a reference to the mall object.
	/// </summary>
	public GameObject mall;
	/// <summary>
	/// Holds a reference to the kiosk object.
	/// </summary>
	public GameObject Kiosk;
	/// <summary>
	/// Initializes the variables needed for this script.
	/// </summary>
	public override void Start () {
		name = type;
	}
	/// <summary>
	/// Activates the event of this object.
	/// </summary>
	public override void ActivateMyEvent ()
	{
		if(type == "Dealer")
		{
			Master.Instance.inUI = true;
			dealer.gameObject.SetActive(true);
		}
		else if(type == "Mall")
		{
			Master.Instance.inUI = true;
			mall.gameObject.SetActive(true);
		}
		else if(type == "Kiosk")
		{
			Master.Instance.inUI = true;
			Kiosk.gameObject.SetActive(true);
		}
	}
}
