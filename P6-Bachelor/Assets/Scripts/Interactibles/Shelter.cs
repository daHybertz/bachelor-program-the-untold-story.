﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// Handles the functionality of the homeless shelter.
/// </summary>
public class Shelter : Interactibles {
	/// <summary>
	/// If the player is admitted to the heating lounge this is true.
	/// </summary>
	bool admitted;
	/// <summary>
	/// Initializes the variables needed for this script.
	/// </summary>
	public override void Start () {
		type = "Shelter";
		name = type;
		admitted = false;
	}
	/// <summary>
	/// Activates the event of this object.
	/// </summary>
	public override void ActivateMyEvent ()
	{
		if(!Master.Instance.FadeOut)
		{
			checkForAdmittance();
		}
	}
	/// <summary>
	/// Calculates the chance that the player will be admitted based on the interaction value. 
	/// And if the result is in his favor, admits him.
	/// </summary>
	void checkForAdmittance()
	{
		float r = UnityEngine.Random.Range(100, 1000);
		r = r/10;
		if(r < Master.Instance.gameValues.getInteractionValue())
		{
			admitted = true;
			OutputText.Instance.NewMessage("Please, do come in.","#FFFFFF");
		}
		else
		{
			OutputText.Instance.NewMessage("We are sorry, there are no more rooms available. Have a nice night.", "#FFFFFF");
		}
		if(admitted)
		{
			resetVars();
		}
	}
	/// <summary>
	/// Resets the core variables to their max, except for drug related values.
	/// </summary>
	void resetVars()
	{
		Master.Instance.FadeOut = true;
		Master.Instance.gameValues.setTired(false);
		Master.Instance.fadeSpeed = 3f;
		Master.Instance.gameValues.setHygiene(10f);
		//This will set it to the max heat value
		Master.Instance.gameValues.setHeat(-0.5f);
		Master.Instance.gameValues.setHeat(0.3f);
		Master.Instance.gameValues.setSustenance(1f);
		admitted = false;
		if(Master.Instance.day)
		{
			DaNCycle.GetComponent<DayAndNightCycle>().SetNight();
		}
		else if(!Master.Instance.day)
		{
			DaNCycle.GetComponent<DayAndNightCycle>().SetDay();
		}
	}
}
