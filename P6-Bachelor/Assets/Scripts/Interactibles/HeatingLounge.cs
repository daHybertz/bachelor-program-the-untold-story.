﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Handles the functionality of the heatinglounge.
/// </summary>
public class HeatingLounge : Interactibles {
	/// <summary>
	/// if the player is admitted to the heating lounge this is true.
	/// </summary>
	bool admitted;
	/// <summary>
	/// Stores the time when last admitted into the shelter.	
	/// </summary>
	public float timeAdmitted;
	/// <summary>
	/// The amount of time to wait before you can enter the shelter again.
	/// </summary>
	public float admissionTimer;

	/// <summary>
	/// Initializes the variables needed for this script.
	/// </summary>
	public override void Start () {
		type = "HeatingLounge";
		name = type;
		timeAdmitted = 0f;
	}
	/// <summary>
	/// Updates the timeAdmitted.
	/// </summary>
	public override void Update()
	{
		if(timeAdmitted > 0f && Master.Instance.gameValues.getTime() != Time.deltaTime)
		{
			timeAdmitted -= Time.deltaTime;
		}
	}
	/// <summary>
	/// Activates the event of this object.
	/// </summary>
	public override void ActivateMyEvent()
	{
		if(!Master.Instance.FadeOut)
		{
			if(timeAdmitted <= 0)
			{
				timeAdmitted = 10f;
				checkForAdmittance();
			}
			else
			{
				OutputText.Instance.NewMessage("We are sorry, but we cannot handle anymore people tonight.","#FFFFFF");
			}
		}
	}
	/// <summary>
	/// Calculates the chance that the player will be admitted based on the interaction value. 
	/// And if the result is in his favor, admits him.
	/// </summary>
	void checkForAdmittance()
	{
		admitted = false;
		float r = UnityEngine.Random.Range(100, 1000);
		r = r/10;
		if(r < Master.Instance.gameValues.getInteractionValue()+20f)
		{
			admitted = true;
			timeAdmitted = admissionTimer;
			OutputText.Instance.NewMessage("Please, do come in.","#FFFFFF");
		}
		else
		{
			OutputText.Instance.NewMessage("We are sorry, we can't handle anymore people tonight.", "#FFFFFF");
		}
		if(admitted)
		{
			resetVars();
		}
	}
	/// <summary>
	/// Resets the core variables to their max, except for drug related values.
	/// </summary>
	void resetVars()
	{
		Master.Instance.FadeOut = true;
		Master.Instance.fadeSpeed  = 3f;
		Master.Instance.gameValues.setHygiene(1f);
		//This will set it to the max heat value
		Master.Instance.gameValues.setHeat(-0.5f);
		Master.Instance.gameValues.setHeat(0.3f);
		Master.Instance.gameValues.setSustenance(1f);
		OutputText.Instance.NewMessage("You feel rejuvenated after a hot meal and a shower.", "0DFF00");
	}
}
