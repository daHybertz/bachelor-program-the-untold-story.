﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Holds functionality for begging the NPC's for help.
/// </summary>
public class Begging : Interactibles {
	/// <summary>
	/// stores the last time this NPC recieved a begging.
	/// </summary>
	public float timeBegged;
	/// <summary>
	/// holds the time until the player can beg again.
	/// </summary>
	public float beggingtimer;
	/// <summary>
	/// Initializes the variables needed for this script.
	/// </summary>
	public override void Start () {
		type = "NPC";
		name = type;
		timeBegged = 0f;
	}
	
	/// <summary>
	/// Updates the timeBegged variable.
	/// </summary>
	public override void Update () {

		if(timeBegged > 0f && Master.Instance.gameValues.getTime() != Time.deltaTime)
		{
			timeBegged -= Time.deltaTime;
		}
	
	}
	/// <summary>
	/// Activates the event of this object.
	/// </summary>
	public override void ActivateMyEvent()
	{
		if(timeBegged <= 0)
		{
			timeBegged += beggingtimer;
			Beg ();
		}
	}
	/// <summary>
	/// Holds the functionality for begging
	/// </summary>
	void Beg()
	{
		float r = UnityEngine.Random.Range(100, 1000);
		r = r/10;
		if(r < Master.Instance.gameValues.getInteractionValue())
		{
			OutputText.Instance.NewMessage("Oh you poor soul, have some change.","#DDDDDD");
			if((int)(r/10) < 1)
			{
				Master.Instance.money += 1;
			}
			else
			{
				Master.Instance.money += Mathf.Clamp((int)(r), 1, 50);
			}
		}
		else
		{
			OutputText.Instance.NewMessage("I don't have any change to give you.", "#DDDDDD");
		}
	}
}
