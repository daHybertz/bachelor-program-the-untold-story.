﻿using UnityEngine;
using System.Collections;
/// <summary>
/// A place to sleep. 
/// Has the varmth of a heating vent, but is more hygienic than the streets. 
/// It is still uncomfortable though.
/// </summary>
public class OutsideShelter : Interactibles {
	/// <summary>
	/// current uses of the shelter.
	/// </summary>
	int uses = 0;
	/// <summary>
	/// The max uses of the shelter.
	/// </summary>
	int maxUses = 5;
	/// <summary>
	/// Activates the event of this object.
	/// </summary>
	public override void ActivateMyEvent()
	{
		if(!Master.Instance.FadeOut && uses < maxUses)
		{
			if(Master.Instance.day)
			{
				DaNCycle.GetComponent<DayAndNightCycle>().SetNight();
			}
			else if(!Master.Instance.day)
			{
				DaNCycle.GetComponent<DayAndNightCycle>().SetDay();
			}
			Master.Instance.FadeOut = true;
			Master.Instance.gameValues.setTired(false);
			Master.Instance.fadeSpeed  = 3f;
			Master.Instance.gameValues.setHygiene(-0.1f);
			//This will set it to the minimum heat value
			Master.Instance.gameValues.setHeat(-0.5f);
			Master.Instance.gameValues.setHeat(0.2f);
			uses++;
			if(uses == (maxUses))
			{
				OutputText.Instance.NewMessage("This tent is ripe for the bin, you probably shouldn't sleep here anymore.", "0DFF00");
			}
			else if(uses > (int)(maxUses-1))
			{
				OutputText.Instance.NewMessage("You feel well rested, but the tent was unclean and you feel dirty.", "0DFF00");
			}
			else if(uses > (int)(maxUses-3))
			{
				OutputText.Instance.NewMessage("You feel well rested, the tent however is getting unhygienic.", "0DFF00");
			}
			else
			{
				OutputText.Instance.NewMessage("You feel well rested,", "0DFF00");
			}
		}
	}
}
