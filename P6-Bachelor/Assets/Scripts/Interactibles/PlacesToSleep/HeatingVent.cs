﻿using UnityEngine;
using System.Collections;
/// <summary>
/// A place to sleep. 
/// Has the varmth of a heating vent, and the discomforts and 
/// hygiene of sleeping on the streets.
/// </summary>
public class HeatingVent : Interactibles {
	/// <summary>
	/// Activates the event of this object.
	/// </summary>
	public override void ActivateMyEvent()
	{
		if(!Master.Instance.FadeOut)
		{
			Master.Instance.FadeOut = true;
			Master.Instance.gameValues.setTired(false);
			Master.Instance.fadeSpeed  = 3f;
			Master.Instance.gameValues.setHygiene(-0.2f);
			//This will set it to the Max heat value
			Master.Instance.gameValues.setHeat(-0.5f);
			Master.Instance.gameValues.setHeat(0.3f);
			OutputText.Instance.NewMessage("You feel well rested, but the street left you quite dirty.", "'0DFF00");
			if(Master.Instance.day)
			{
				DaNCycle.GetComponent<DayAndNightCycle>().SetNight();
			}
			else if(!Master.Instance.day)
			{
				DaNCycle.GetComponent<DayAndNightCycle>().SetDay();
			}
		}
	}
}
