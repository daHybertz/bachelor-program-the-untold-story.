﻿using UnityEngine;
using System.Collections;
/// <summary>
/// A place to sleep.
/// It's cold, unhygienic and uncomfortable.
/// </summary>
public class Street : Interactibles {
	/// <summary>
	/// Activates the event of this object.
	/// </summary>
	public override void ActivateMyEvent()
	{
		if(!Master.Instance.FadeOut)
		{
			if(Master.Instance.sleepingBagOnPerson < 1)
			{
				Master.Instance.FadeOut = true;
				Master.Instance.gameValues.setTired(false);
				Master.Instance.fadeSpeed  = 3f;
				Master.Instance.gameValues.setHygiene(-0.2f);
					//This will set it to the minimum heat value
				Master.Instance.gameValues.setHeat(-0.5f);
				OutputText.Instance.NewMessage("You feel rested, the sleeping bag got the most of it, " +
					"but sleeping on the street is dirty & unhealthy. " +
				    "Perhaps a shower and some hot food somewhere?", "0DFF00");
			}
			else 
			{ 
				Master.Instance.FadeOut = true;
				Master.Instance.gameValues.setTired(false);
				Master.Instance.fadeSpeed  = 3f;
				Master.Instance.gameValues.setHygiene(-0.1f);
				//This will set it to the minimum heat value
				Master.Instance.gameValues.setHeat(-0.5f);
				Master.Instance.gameValues.setHeat(0.1f);
			}
			if(Master.Instance.day)
			{
				DaNCycle.GetComponent<DayAndNightCycle>().SetNight();
			}
			else if(!Master.Instance.day)
			{
				DaNCycle.GetComponent<DayAndNightCycle>().SetDay();
			}
		}
	}
}
