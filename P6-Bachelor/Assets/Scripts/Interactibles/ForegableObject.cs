﻿using UnityEngine;
using System.Collections;
/// <summary>
/// handles the foregable objects. Foraging food & bottles for income.
/// </summary>
public class ForegableObject : Interactibles {
	/// <summary>
	/// Sets the time this specific trash can was last foraged.
	/// </summary>
	public float timeForaged;
	/// <summary>
	/// keeps track of how long it's been since last foraging.
	/// </summary>
	public float timer;
	/// <summary>
	/// Initializes the variables needed for this script.
	/// </summary>
	public override void Start () {
		type = "Trash";
		name = type;
		timeForaged = 0f;
	}
	
	/// <summary>
	/// Updates timeForaged.
	/// </summary>
	public override void Update () {
		if(timeForaged > 0f && Master.Instance.gameValues.getTime() != Time.deltaTime)
		{
			timeForaged -= Time.deltaTime;
		}
	
	}
	/// <summary>
	/// Activates the event of this object.
	/// </summary>
	public override void ActivateMyEvent ()
	{
		if(timeForaged <= 0f)
		{
			float r = UnityEngine.Random.Range(100, 1000);
			r = r/10;
			timeForaged += timer;
			if(r > 30)
			{
				OutputText.Instance.NewMessage("Gained a bottle for recycling.","#999999");
				Master.Instance.bottles++;
				if(r > 70)
				{
					OutputText.Instance.NewMessage("Gained some food.","#999999");
					Master.Instance.trashOnPerson++;
				}
			}
			else if(r < 30)
			{
				OutputText.Instance.NewMessage("Gained some food.","#999999");
				Master.Instance.trashOnPerson++;
			}
			else
			{
				OutputText.Instance.NewMessage("It's empty.", "#F999999");
			}
		}
		else
		{
			OutputText.Instance.NewMessage("It's empty. You just rumaged through it", "#999999");
		}
	}
}
