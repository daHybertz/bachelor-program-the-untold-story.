﻿using UnityEngine;
using System.Collections;
/// <summary>
/// A script made for buttonpush functionality.
/// </summary>
public class ActiveTestSystem : MonoBehaviour {
	/// <summary>
	/// If true, test system UI is active.
	/// </summary>
	bool activateTest;
	public void ActivateMe()
	{
		activateTest = !activateTest;
		this.gameObject.SetActive(activateTest);
	}
	/// <summary>
	/// allows for opening the test system even though UI button isn't there. Press t to open/close it.
	/// </summary>
	public void Update()
	{
		if(Input.GetKeyDown(KeyCode.t))
		{
			activateTest = !activateTest;
			this.gameObject.SetActive(activateTest);
		}
	}
}
