using UnityEngine;
using System.Collections;
/// <summary>
/// A script made for buttonpush functionality.
/// </summary>
public class TempSustenance : MonoBehaviour {
	/// <summary>
	/// Eats food on person as long as he has any on him.
	/// </summary>
	public void EatMeals()
	{
		if(Master.Instance.storeboughtFood <= 1)
		{
			this.gameObject.SetActive(false);
		}
		Master.Instance.storebought.Use (ref Master.Instance.storeboughtFood);
	}
	/// <summary>
	/// Eats trash food on person as long as he has any on him.
	/// </summary>
	public void EatTrash()
	{
		if(Master.Instance.trashOnPerson <= 1)
		{
			this.gameObject.SetActive(false);
		}
		Master.Instance.trash.Use (ref Master.Instance.trashOnPerson);
	}
}
