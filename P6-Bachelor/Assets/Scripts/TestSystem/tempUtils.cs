﻿using UnityEngine;
using System.Collections;
/// <summary>
/// A script made for buttonpush functionality.
/// </summary>
public class tempUtils : MonoBehaviour {
	/// <summary>
	/// Cleans up the player based on the cleanup utilities he has on him.
	/// </summary>
	public void cleanUp()
	{
		Master.Instance.deo.Use(ref Master.Instance.deoOnPerson);
		Master.Instance.shaver.Use (ref Master.Instance.shaverOnPerson);
		Master.Instance.soap.Use (ref Master.Instance.sleepingBagOnPerson);
		Master.Instance.toothbrush.Use(ref Master.Instance.toothbrushOnPerson);
	}
}
