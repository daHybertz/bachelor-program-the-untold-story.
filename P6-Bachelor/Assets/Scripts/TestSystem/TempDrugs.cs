﻿using UnityEngine;
using System.Collections;
/// <summary>
/// A script made for buttonpush functionality.
/// </summary>
public class TempDrugs : MonoBehaviour{
	/// <summary>
	/// Drug button. This will intoxicate the player with one dose of drugs, 
	/// as long as the player has drugs on his person.
	/// </summary>
	public void DoDrugs()
	{
		if(Master.Instance.drugsOnPerson <= 1)
		{
			this.gameObject.SetActive(false);
		}
		Master.Instance.drugs.Use(ref Master.Instance.drugsOnPerson);
		Master.Instance.gameValues.setDetoxProxy();

	}
	/// <summary>
	/// Alcohol button. This will intoxicate the player with one dose of alcohol, 
	/// as long as the player has alcohol on his person.
	/// </summary>
	public void DoAlco()
	{
		if(Master.Instance.alcoOnPerson <= 1)
		{
			this.gameObject.SetActive(false);
		}
		Master.Instance.alcohol.Use(ref Master.Instance.alcoOnPerson);
		Master.Instance.gameValues.setDetoxProxy();
	}

}
