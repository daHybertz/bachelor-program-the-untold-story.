﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
/// <summary>
/// Handles the textupdates for the test system.
/// All the variables are updated each frame, 
/// then it's loaded into a string and that string is loaded into a textfield and thereby displayed.
/// </summary>
public class Tempvalues : MonoBehaviour {

	float health;
	float bleakness;
	float heat;
	float hygiene;
	float toxicity;
	float drugUse;
	float sustenance;
	int drugUnits;
	int currDrugUnits;
	float time;
	float detoxTime;
	float detoxProxy;
	float timeScaleValue;
	float interactionValue;
	float interactionMOD;
	float toxTime;

	// Use this for initialization
	void Start () {
	
	}
	
	/// <summary>
	/// Updates the string for test system text box.
	/// </summary>
	void Update () {
			
		UpdateCoreValues();
		gameObject.GetComponent<Text>().text = "Health: "+ health+"&Bleakness: "+bleakness
			+"&Heat: "+heat+"&Hygiene: "+hygiene
			+"&Sustenance: "+sustenance+"&Time: "+time+"&Time Scale Value: "+timeScaleValue
			+"&Interaction Value: "+interactionValue+"&Interaction Modifier: "+interactionMOD

				+"&Toxicity: "+toxicity+"&Drug Use: "+drugUse+"&Detox Time: "+detoxTime 
				+ "&Tox Time: "+toxTime +"&Detox Proxy: "+detoxProxy
			+"&Drug Units: "+drugUnits+"&Current Drug Units: "+currDrugUnits;

		gameObject.GetComponent<Text>().text = gameObject.GetComponent<Text>().text.Replace("&","\n");
	}
	/// <summary>
	/// Updates the core values.
	/// </summary>
	void UpdateCoreValues()
	{
		toxTime = Master.Instance.gameValues.getToxTime();
		detoxProxy = Master.Instance.gameValues.getDetoxProxy();
		health = Master.Instance.gameValues.getHealth();
		bleakness = Master.Instance.gameValues.getBleakness();
		heat = Master.Instance.gameValues.getHeat();
		hygiene = Master.Instance.gameValues.getHygiene();
		toxicity = Master.Instance.gameValues.getToxicity();
		drugUse = Master.Instance.gameValues.getDrugUse();
		sustenance = Master.Instance.gameValues.getSustenance();
		drugUnits = Master.Instance.gameValues.getDrugUnits();
		currDrugUnits = Master.Instance.gameValues.getCurrDrugUnits();
		time = Master.Instance.gameValues.getTime();
		detoxTime = Master.Instance.gameValues.getDetoxTime();
		timeScaleValue = Master.Instance.gameValues.getTimeScaleValue();
		interactionValue = Master.Instance.gameValues.getInteractionValue();
		interactionMOD = Master.Instance.gameValues.getInteractionMOD();
	}
}