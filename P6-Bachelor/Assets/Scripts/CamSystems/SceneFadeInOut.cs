﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
/// <summary>
/// Handles the scene fade in & out.
/// </summary>
public class SceneFadeInOut : MonoBehaviour
{   
	/// <summary>
	/// if this is the true the screen fades in.
	/// </summary>
	bool FadeIn;
	/// <summary>
	/// when this variable is true, the game ends.
	/// </summary>
	bool endOfGame;
	/// <summary>
	/// Holds the reference to the image that fades onto the screen.
	/// </summary>
	public GameObject fff;
	/// <summary>
	/// if this is true, the scene is starting.
	/// </summary>
	private bool sceneStarting = true;
	
	/// <summary>
	/// initializes the variables needed for this script.
	/// </summary>
	void Awake ()
	{
		// Set the texture so that it is the the size of the screen and covers it.
		GetComponent<Image>().enabled =true;
		endOfGame = false;
	}
	
	/// <summary>
	/// Updates the fade variables based on the fade in and fade out variables.
	/// </summary>
	void Update ()
	{
		// If the scene is starting...
		if(sceneStarting)
			// ... call the StartScene function.
			StartScene();
		if(Master.Instance.FadeOut && !FadeIn)
		{
			GetComponent<Image>().enabled =true;
			FadeToBlack();
			if(GetComponent<Image>().color.a >= 0.95f)
			{
				Master.Instance.FadeOut = !Master.Instance.FadeOut;
				FadeIn = !FadeIn;
			}
		}
		else if(FadeIn && !Master.Instance.FadeOut)
		{
			FadeToClear();
			if(GetComponent<Image>().color.a <= 0.05f)
			{
				// ... set the colour to clear and disable the GUITexture.
				GetComponent<Image>().color = Color.clear;
				GetComponent<Image>().enabled = false;
				Master.Instance.fadeSpeed = 1.5f;
				FadeIn = false;
				Master.Instance.FadeOut = false;
			}
		}
		if(Master.Instance.gameValues.getHealth() < 15f || Master.Instance.gameValues.getTime() > 900f)
		{
			Master.Instance.endGame = true;

			if(!endOfGame)
			{
				OutputText.Instance.NewMessage("EndingGame","#FF00FF");
				endOfGame = true;
			}
			EndScene();
		}
	}

	/// <summary>
	/// Fades the screen in.
	/// </summary>
	public void FadeToClear ()
	{
		// Lerp the colour of the texture between itself and transparent.
		GetComponent<Image>().color = Color.Lerp(GetComponent<Image>().color, Color.clear, Master.Instance.fadeSpeed * Time.deltaTime);
	}
	
	/// <summary>
	/// Fades the screen out.
	/// </summary>
	public void FadeToBlack ()
	{
		// Lerp the colour of the texture between itself and black.
		GetComponent<Image>().color = Color.Lerp(GetComponent<Image>().color, Color.black, Master.Instance.fadeSpeed * Time.deltaTime);
	}
	
	/// <summary>
	/// fades the screen in at the start of the scene.
	/// </summary>
	public void StartScene ()
	{
		Master.Instance.fadeSpeed = 1.5f;
		// Fade the texture to clear.
		FadeToClear();
		
		// If the texture is almost clear...
		if(GetComponent<Image>().color.a <= 0.05f)
		{
			// ... set the colour to clear and disable the GUITexture.
			GetComponent<Image>().color = Color.clear;
			GetComponent<Image>().enabled = false;
			
			// The scene is no longer starting.
			sceneStarting = false;
		}
	}
	
	/// <summary>
	/// fades the screen out at the end of the game.
	/// </summary>
	public void EndScene ()
	{

		Debug.Log ("Ending Game");
		// Make sure the texture is enabled.
		GetComponent<Image>().enabled = true;
		FadeIn = false;
		Master.Instance.FadeOut = false;
		Master.Instance.fadeSpeed = 1.5f;
		
		// Start fading towards black.
		FadeToBlack();
		
		// If the screen is almost black...
		if(GetComponent<Image>().color.a >= 0.95f)
			fff.gameObject.SetActive(true);

	}
}