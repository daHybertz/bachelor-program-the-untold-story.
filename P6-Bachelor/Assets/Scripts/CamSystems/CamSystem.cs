﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Controls the main camera.
/// </summary>
public class CamSystem : MonoBehaviour {
	/// <summary>
	/// Holds a reference to the player
	/// </summary>
	GameObject Player;
	/// <summary>
	/// If this is false, then the camera can't move vertically.
	/// </summary>
	public bool canMoveVertical;
	/// <summary>
	/// if this is false, then the camera can't move horizontally.
	/// </summary>
	public bool canMoveHorizontal;
	/// <summary>
	/// defines where the center of the camera should be.
	/// </summary>
	Vector3 center;
	/// <summary>
	/// Initializes the variables needed for this script
	/// </summary>
	void Start () {
		Player = GameObject.FindGameObjectWithTag("Player");
		canMoveVertical = true;
		canMoveHorizontal = true;
	}
	/// <summary>
	/// Updates the position of the camera.	
	/// </summary>
	void Update () {
		if(canMoveVertical && canMoveHorizontal)
		{
			center = new Vector3(0,0,-3.548449f); // center on his position, set Z so it is encompassing player and gameworld.
				// update center with new position of player.
			center += Player.transform.position; 
				// center y so camera is stationary.
			center.y = Player.transform.position.y; 
				// make the camera lerp to give a smooth movement feeling.
			transform.position = Vector3.Slerp(transform.position, center, 0.8f*Time.deltaTime); 
		}
		else if(canMoveVertical && !canMoveHorizontal)
		{
			center = new Vector3(center.x,0,-3.548449f); // center on his position, set Z so it is encompassing player and gameworld.
			center.y = Player.transform.position.y;
			transform.position = Vector3.Slerp(transform.position, center, 0.8f*Time.deltaTime); 
		}
		else if(!canMoveVertical && canMoveHorizontal)
		{
			center = new Vector3(0,center.y,-3.548449f); // center on his position, set Z so it is encompassing player and gameworld.
			center.x = Player.transform.position.x;
			transform.position = Vector3.Slerp(transform.position, center, 0.8f*Time.deltaTime); 
		}
		else
		{
			Debug.Log ("Im STUCK!");
		}
	
	}

}
