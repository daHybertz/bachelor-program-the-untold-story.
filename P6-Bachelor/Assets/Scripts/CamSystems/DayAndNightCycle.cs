﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
/// <summary>
/// handles the day and night cycle.
/// </summary>
public class DayAndNightCycle : MonoBehaviour {
	/// <summary>
	/// The fade speed of the day and night cycle.
	/// </summary>
	public float fadeSpeed = 1.5f;
	/// <summary>
	/// if this is true, the object will fade to night.
	/// </summary>
	public bool fadeToNight;
	/// <summary>
	/// if this is ture, the object will fade to day.
	/// </summary>
	public bool fadeToDay;
	
	/// <summary>
	/// Updates the day & night cycles.	/// </summary>
	void Update ()
	{
		if(fadeToNight)
		{

			FadeToNight();
			if(GetComponent<Image>().color.a >= 0.31f)
			{
				Master.Instance.day = false;
				fadeToNight = !fadeToNight;
				fadeToDay = true;
			}
		}
		else if(fadeToDay)
		{
			FadeToDay();
			if(GetComponent<Image>().color.a <= 0.05f)
			{
				Master.Instance.day = true;
				fadeToDay = !fadeToDay;
				fadeToNight = true;
				GetComponent<Image>().color = Color.clear;
				//GetComponent<Image>().enabled = false;
			}
		}
	}
	
	/// <summary>
	/// Fades to day.
	/// </summary>
	public void FadeToDay ()
	{
		// Lerp the colour of the texture between itself and transparent.
		GetComponent<Image>().color = Color.Lerp(GetComponent<Image>().color, Color.clear, (fadeSpeed*2) * Time.deltaTime);
	}
	
	/// <summary>
	/// Fades to night.
	/// </summary>
	public void FadeToNight ()
	{
		// Lerp the colour of the texture between itself and black.
		GetComponent<Image>().color = Color.Lerp(GetComponent<Image>().color, new Color(0f,0f,0.58f,0.4f), fadeSpeed * Time.deltaTime);
	}
	/// <summary>
	/// Sets varaibles to day.
	/// </summary>
	public void SetDay()
	{
		Master.Instance.day = true;
		fadeToDay = !fadeToDay;
		fadeToNight = !fadeToNight;
		GetComponent<Image>().color = Color.clear;
	}
	/// <summary>
	/// Sets the variables to night.
	/// </summary>
	public void SetNight()
	{
		Master.Instance.day = false;
		fadeToDay = !fadeToDay;
		fadeToNight = !fadeToNight;
		GetComponent<Image>().color = new Color(0f,0f,0.58f,0.4f);
	}
}
