﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Second cam system. 
/// Handles the size of the second camera, 
/// rendering the color objects.
/// </summary>
public class SecondCamSystem : MonoBehaviour {
	
	/// <summary>
	/// makes sure the second camera has the same size as the main camera.
	/// </summary>
	void Update () {
	
		this.gameObject.GetComponent<Camera>().orthographicSize = Camera.main.orthographicSize;
	}
}
